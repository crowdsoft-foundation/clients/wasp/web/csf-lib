import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {getCookie} from "./pb-functions.js?v=[cs_version]"

export class pbLinkResolver {
    resolver_functions = {}
    data = {}
    
    constructor() {
        if (!pbLinkResolver.instance) {
            pbLinkResolver.instance = this;
            pbLinkResolver.instance.resolver_functions={
                                                        "contact-manager": {"contact": pbLinkResolver.instance.resolveContact},
                                                        "easy2schedule": {"event": pbLinkResolver.instance.resolveEvent}
                                                    }
        }
        return pbLinkResolver.instance;
    }

    getData(linkList) {
        let buckets = {}
        let promise = new Promise(function (resolve, reject) {
            for (let i = 0; i < linkList.length; i++) {
                if(!buckets[linkList[i].resolver]) {
                    buckets[linkList[i].resolver] = []
                }
                if(!buckets[linkList[i].resolver][linkList[i].type]) {
                    buckets[linkList[i].resolver][linkList[i].type] = []
                }
                buckets[linkList[i].resolver][linkList[i].type].push(linkList[i].id)
            }
            
            let promises = []
            for(let resolver in buckets) {
                for(let type in buckets[resolver]) {
                    if(pbLinkResolver.instance.resolver_functions[resolver] && pbLinkResolver.instance.resolver_functions[resolver][type]) {
                        promises.push(pbLinkResolver.instance.resolver_functions[resolver][type](buckets[resolver][type]))
                    } else {
                        if(type != "remove") {
                            console.warn("No resolver function for " + resolver + " " + type)
                        }
                    }
                    
                }
            }
            Promise.all(promises).then(function (res) {
                let result = {}
                for(let i = 0; i < res.length; i++) {
                    for(let resolver in res[i]) {
                        for(let type in res[i][resolver]) {
                            if(!result[resolver]) {
                                result[resolver] = {}
                            }
                            if(!result[resolver][type]) {
                                result[resolver][type] = []
                            }
                            result[resolver][type] = res[i][resolver][type]
                        }
                    }
                }
                resolve(result)
            })
        })
        return promise
    }

    resolveContact(ids) {
        let promise = new Promise(function (resolve, reject) {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/contacts/list",
            "method": "GET",
            "timeout": 0,
            "headers": {
              "apikey": getCookie("apikey")
            },
            "data": ids,
            "success": function (response) { 
                let result = {"contact-manager": {"contact": {}}}
                
                for(let contact of response) {
                    if(ids.includes(contact.contact_id)) {
                        result["contact-manager"]["contact"][contact.contact_id] = contact
                    }
                }
                
                resolve(result)
             },
            "error": function (error) {}
          };

          $.ajax(settings)
        })
        return promise
    }

    resolveEvent(ids) {
        let idString 
        if(ids.length > 1) {
            idString = ids.join(",")
        }
        else{
            idString = ids[0]
        }
        let promise = new Promise(function (resolve, reject) {

        if(!idString) {
            reject()
            return
        }
        var settings = {
            "url": CONFIG.API_BASE_URL + "/cb/event/" + idString,
            "method": "GET",
            "timeout": 0,
            "headers": {
              "apikey": getCookie("apikey")
            },
/*             "data": ids, */
            "success": function (response) { 
                if(typeof response == "object") {
                    response = [response]
                }

                let result = {"easy2schedule": {"event": {}}}
                for(let event of response) {
                    if(ids.includes(event.event_id)) {
                        result["easy2schedule"]["event"][event.event_id] = event
                    }
                }
                
                resolve(result)
             },
            "error": function (xhr, thrownError){
                if(xhr.status==404) {
                    console.warn("Event assigned to this task not found", thrownError);
                }
            }
          };
          
          $.ajax(settings)
        })
        return promise
    }
}
