import {pbI18n} from "./pb-i18n.js?v=[cs_version]";


export class PbNotifications {
    constructor() {
        if (!PbNotifications.instance) {
            PbNotifications.instance = this
            PbNotifications.instance.notifications = {}
            PbNotifications.instance.intervals = {}
            PbNotifications.instance.timeouts = {}
            PbNotifications.instance.box_blocked_selectors = ['.az-content-body']
            PbNotifications.instance.initListeners()
        }

        return PbNotifications.instance
    }

    initListeners() {
        document.addEventListener("notification", function _(event) {
            if (event.data.data.args.correlation_id) {
                PbNotifications.instance.notifications[event.data.data.args.correlation_id] = event.data
            }
        })
    }

    async test() {
        return await Promise.resolve(1);
    }

    waitForCorrelationId(correlation_id, auto_delete = true, timeout = 10000) {
        let interval = 100
        return new Promise((resolve, reject) => {
            if (PbNotifications.instance.notifications[correlation_id]) {
                let notification = PbNotifications.instance.getNotification(correlation_id, auto_delete)
                if (auto_delete) {
                    delete PbNotifications.instance.notifications[correlation_id]
                }
                resolve(notification)
            } else {
                PbNotifications.instance.timeouts[correlation_id] = 0
                PbNotifications.instance.intervals[correlation_id] = setInterval(() => {
                    PbNotifications.instance.timeouts[correlation_id] += interval
                    if (PbNotifications.instance.notifications[correlation_id]) {
                        let notification = PbNotifications.instance.getNotification(correlation_id, auto_delete)
                        if (auto_delete) {
                            delete PbNotifications.instance.notifications[correlation_id]
                        }
                        clearInterval(PbNotifications.instance.intervals[correlation_id])
                        resolve(notification)
                    } else {
                        if (PbNotifications.instance.timeouts[correlation_id] >= timeout) {
                            clearInterval(PbNotifications.instance.intervals[correlation_id])
                            reject(new Error("Timeout"))
                        }
                    }
                }, interval)
            }
        })
    }

    getNotification(correlation_id, auto_delete = true) {
        let notification = PbNotifications.instance.notifications[correlation_id]
        if (notification && auto_delete) {
            PbNotifications.instance.remove(correlation_id)
        }

        return notification
    }

    showAlert(message, title, type = "error", callback = () => "") {

        switch (type) {
            case "error":
                Fnon.Alert.Danger(new pbI18n().translate(message), new pbI18n().translate(title), new pbI18n().translate('Okay'), (value) => {
                    callback()
                });
                break;
            case "warning":
                Fnon.Alert.Warning(new pbI18n().translate(message), new pbI18n().translate(title), new pbI18n().translate('Okay'), (value) => {
                    callback()
                });
                break;
            case "success":
                Fnon.Alert.Success(new pbI18n().translate(message), new pbI18n().translate(title), new pbI18n().translate('Okay'), (value) => {
                    callback()
                });
                break;
        }
    }

    showHint(message, type = "success", callback = () => "", displayDuration = 5000) {
        switch (type) {
            case "error":
                Fnon.Hint.Danger(new pbI18n().translate(message), {displayDuration: displayDuration, callback: callback})
                break;
            case "warning":
                Fnon.Hint.Warning(new pbI18n().translate(message), {displayDuration: displayDuration, callback: callback})
                break;
            case "success":
                Fnon.Hint.Success(new pbI18n().translate(message), {displayDuration: displayDuration, callback: callback})
                break;
        }
    }

    blockForLoading(selector = '.az-content-body') {
        PbNotifications.instance.box_blocked_selectors.push(selector)
        Fnon.Box.Circle(selector, '', {
            svgSize: {w: '50px', h: '50px'},
            svgColor: '#3a22fa',
        })
    }

    unblock(selector = '.az-content-body') {
        if (PbNotifications.instance.box_blocked_selectors.includes(selector)) {
            PbNotifications.instance.box_blocked_selectors.remove(selector)
            try {
                Fnon.Box.Remove(selector)
            } catch(e) {

            }
        }
    }

    unblockAll() {
        PbNotifications.instance.box_blocked_selectors.forEach(selector => {
            try {
                Fnon.Box.Remove(selector)
            } catch (e) {
                // Fail silently since we just want to make sure all existing boxes are removed
            }
        })
    }

    ask(question, title = "", okay_callback = () => "", abort_callback = () => "", type = "warning", okay_string = "Okay", cancel_string = "Cancel") {
        switch (type) {
            case "danger":
                Fnon.Ask.Danger(new pbI18n().translate(question), new pbI18n().translate(title), new pbI18n().translate(okay_string), new pbI18n().translate(cancel_string), (result) => {
                    result ? okay_callback() : abort_callback()
                })
                break
            case "warning":
                Fnon.Ask.Warning(new pbI18n().translate(question), new pbI18n().translate(title), new pbI18n().translate(okay_string), new pbI18n().translate(cancel_string), (result) => {
                    result ? okay_callback() : abort_callback()
                })
                break
            case "info":
                Fnon.Ask.Info(new pbI18n().translate(question), new pbI18n().translate(title), new pbI18n().translate(okay_string), new pbI18n().translate(cancel_string), (result) => {
                    result ? okay_callback() : abort_callback()
                })
                break
            case "success":
                Fnon.Ask.Success(new pbI18n().translate(question), new pbI18n().translate(title), new pbI18n().translate(okay_string), new pbI18n().translate(cancel_string), (result) => {
                    result ? okay_callback() : abort_callback()
                })
                break
        }
    }

    remove(correlation_id) {
        delete PbNotifications.instance.notifications[correlation_id]
    }
}