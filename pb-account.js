import {CONFIG} from "../pb-config.js?v=[cs_version]"
import {getCookie, setCookie, isEmail, makeId, showNotification, getQueryParam} from "./pb-functions.js?v=[cs_version]"
import {fadeIn} from "./pb-formchecks.js?v=[cs_version]"
import {pbPermissions} from "./pb-permission.js?v=[cs_version]"
import {fireEvent} from "./pb-functions.js?v=[cs_version]"
import {PbSubscriptions} from "./pbapi/accountmanager/pb-subscriptions.js?v=[cs_version]"

export class PbAccount {
    permissions = {}
    accountProfileData = {}
    loaded_account_id = ""
    accountProfileTemplate = {
        "account_id": "",
        "data": {}
    }
    subscriptions=[]


    constructor(silentMode = false) {
        if (getQueryParam("apikey") && !getCookie("apikey")) {
            setCookie("apikey", getQueryParam("apikey"), {path: '/'})
            setCookie("consumer_id", getQueryParam("consumer_id"), {path: '/'})
            setCookie("consumer_name", getQueryParam("consumer_name"), {path: '/'})
            setCookie("username", getQueryParam("username"), {path: '/'})
            let url = new URL(location.href);
            url.searchParams.delete('apikey');
            url.searchParams.delete('consumer_id');
            url.searchParams.delete('consumer_name');
            url.searchParams.delete('username');
            location.href = url
        } else {
            if (!PbAccount.instance) {
                PbAccount.instance = this
                PbAccount.instance.silentMode = silentMode
                PbAccount.instance.initListeners()
                PbAccount.instance.subscriptions = new PbSubscriptions(undefined)
                new pbPermissions()
                    .getAbsoluteRightsOfRole("user." + getCookie("username")).then(
                    (response) => {
                        if (response != undefined) {
                            PbAccount.instance.permissions = response.rules
                            PbAccount.instance.handlePermissionAttributes()
                        }
                    })
            }

            return PbAccount.instance
        }
    }

    init(account_id = getCookie("consumer_id"), force_reload = false) {
        return new Promise(function (resolve, reject) {
            new pbPermissions()
                .getAbsoluteRightsOfRole("user." + getCookie("username")).then(
                (response) => {
                    if (response != undefined) {
                        PbAccount.instance.permissions = response.rules
                        PbAccount.instance.handlePermissionAttributes()

                        if (account_id == undefined) {
                            resolve(PbAccount.instance.accountProfileTemplate)
                            return
                        }

                        if (account_id == PbAccount.instance.loaded_account_id && force_reload == false) {
                            resolve(PbAccount.instance.accountProfileData)

                        } else {
                            var arr = [PbAccount.instance.getLogins(), PbAccount.instance.fetchAccountProfileData(account_id), PbAccount.instance.subscriptions.loadFromApi()]
                            Promise.all(arr)
                                .then(function (response) {
                                    PbAccount.instance.loaded_account_id = account_id
                                    PbAccount.instance.logins = response[0]
                                    PbAccount.instance.setAccountProfileData(response[1])
                                    
                                    resolve(PbAccount.instance.accountProfileData)
                                })
                                .catch(function (err) {
                                    reject()
                                })

                        }
                    }
                })


        })

    }

    refreshPermissions() {
        new pbPermissions()
            .getAbsoluteRightsOfRole("user." + getCookie("username")).then(
            (response) => {
                if (response) {
                    PbAccount.instance.permissions = response.rules
                    PbAccount.instance.handlePermissionAttributes()
                }
            })
    }

    hasPermission(permission) {
        if (PbAccount.instance.permissions[permission] != undefined
            && PbAccount.instance.permissions[permission].action.includes("allow")
        ) {
            return true
        } else {
            return false
        }
    }

    handlePermissionAttributes() {
        $("[data-permission='']").removeClass('hidden')
        for (let [permission, value] of Object.entries(PbAccount.instance.permissions)) {
            if (PbAccount.instance.hasPermission(permission)) {
                $("[data-permission='" + permission + "']").removeClass('hidden')
                $("[data-permission='!" + permission + "']").addClass('hidden')
            } else {
                $("[data-permission='" + permission + "']").addClass('hidden')
                $("[data-permission='!" + permission + "']").removeClass('hidden')
            }
        }
    }

    sendInvitationLink(email, success = () => {
        showNotification("Invitation sent")
    }, error = (message) => {
        showNotification("Invitation sending failed")
    }) {
        return new Promise(function (resolve, reject) {
            if (!isEmail(email)) {
                error("not an email address")
                return false
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + "/login_invite",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                },
                "data": JSON.stringify({"username": email, "basedomain": window.location.origin}),
                "success": function (response) {
                    success(response);
                    resolve(response)
                },
                "error": function (response) {
                    error(response);
                    reject(response)
                }
            }

            window.jQuery.ajax(settings)
        })
    }

    startRegistrationProcess(login, email, password, successCallback, errorCallback) {
        return new Promise(function (resolve, reject) {
            var data = {"username": login, "email": email, "password": password}

            Fnon.Box.Circle('form', '', {
                svgSize: {w: '50px', h: '50px'},
                svgColor: '#3a22fa',
            })

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": CONFIG.API_BASE_URL + "/registration",
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                },
                "processData": false,
                "data": JSON.stringify(data),
                "success": function (response) {
                    successCallback(response);
                    resolve(response)
                },
                "error": function (response) {
                    errorCallback(response);
                    reject(response)
                }
            }

            window.jQuery.ajax(settings)
        })
    }

    startLostPasswordProcess(login, successCallback, errorCallback) {
        return new Promise(function (resolve, reject) {
            var data = {"username": login, "basedomain": window.location.origin}

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": CONFIG.API_BASE_URL + "/init_credentials_lost_process",
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                },
                "processData": false,
                "data": JSON.stringify(data),
                "success": function (response) {
                    successCallback(response);
                    resolve(response)
                },
                "error": function (response) {
                    errorCallback(response);
                    reject(response)
                }
            }

            window.jQuery.ajax(settings)
        })
    }

    getLogins(callback = () => "") {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_users",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                }
            }

            window.jQuery.ajax(settings).done(function (response) {
                resolve(response)
                callback(response)
            })
        })
    }

    deleteAccount(consumer_id, callback = () => "") {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteAccount",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                },
                "data": JSON.stringify({"id": consumer_id}),
            }

            window.jQuery.ajax(settings).done(function (response) {
                resolve(response)
                callback(response)
            })
        })
    }

    deleteLogin(loginName, callback = () => "") {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteLogin",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey")
                },
                "data": JSON.stringify({"username": loginName}),
            }

            window.jQuery.ajax(settings).done(function (response) {
                resolve(response)
                callback(response)
            })
        })
    }

    createLogin(username, password, roles = undefined, successCallback = () => {
        alert("Login creation successfull")
    }, errorCallback = () => {
        alert("Login creation failed")
    }) {
        let payload = {"username": username.trim(), "password": password.trim()}
        if (roles) {
            payload["roles"] = roles
        }
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/newlogin",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": window.jQuery.cookie("apikey"),
                },
                "data": JSON.stringify(payload),
                "error": errorCallback
            }

            window.jQuery.ajax(settings).done(function (response) {
                successCallback()
                document.addEventListener("notification", function _(event) {
                    if (event.data.data.args.correlation_id == response.taskid) {
                        if (event.data.data.event == "loginCreationFailed") {
                            reject()
                            errorCallback()
                            console.log("loginCreationFailed", event.data.data.response)
                        }
                        if (event.data.data.event == "loginCreated") {
                            resolve()
                            successCallback()
                        }

                        document.removeEventListener("notification", _)
                    }
                })
            })
        })
    }

    checkLogin(isLoginPage = false, checkagainTime = 30000, health_endpoint = "/has_valid_login") {
        return new Promise(function (resolve, reject) {
            console.info("Checking login...")
            let url_for_redirect_after_login = CONFIG.LOGIN_URL + '?r=' + window.location.href
            let apikey = window.jQuery.cookie("apikey")
            if (typeof (apikey) == "undefined") {
                let redirect_file = "index.html"
                let current_url = window.location.pathname
                let current_filename = current_url.substring(current_url.lastIndexOf('/') + 1)

                if (redirect_file != current_filename) {
                    if (!new PbAccount().silentMode) {
                        window.jQuery.blockUI({
                            blockMsgClass: 'alertBox',
                            message: 'Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...',
                            timeout: 3000,
                            baseZ: 9999999999999999,
                            onUnblock: function () {
                                $(location).attr('href', url_for_redirect_after_login)
                            }
                        })
                    }
                }
            } else {
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": CONFIG.API_BASE_URL + health_endpoint,
                    "method": "GET",
                    "headers": {
                        "apikey": window.jQuery.cookie("apikey"),
                        "Cache-Control": "no-cache"
                    },
                    "success": function (response) {
                        //setTimeout(PbAccount.instance.checkLogin, checkagainTime, isLoginPage, checkagainTime)
                        if (isLoginPage) {
                            PbAccount.instance.redirectDialog()
                        }
                        resolve(response)
                    },
                    "error": function (xmlhttprequest, textstatus, message) {
                        if (textstatus != "error" || xmlhttprequest.status == 401) {
                            console.error("LOGINCHECK ERROR RESULT", textstatus, message)
                            if (!isLoginPage) {
                                window.jQuery.removeCookie("apikey", {path: '/'})
                                window.jQuery.removeCookie("consumer_id", {path: '/'})
                                window.jQuery.removeCookie("consumer_name", {path: '/'})
                                window.jQuery.removeCookie("username", {path: '/'})

                                if (!new PbAccount().silentMode) {
                                    window.jQuery.blockUI({
                                        blockMsgClass: 'alertBox',
                                        message: 'Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...',
                                        timeout: 3000,
                                        baseZ: 9999999999999999,
                                        onUnblock: function () {
                                            $(location).attr('href', url_for_redirect_after_login)
                                        }
                                    })
                                }
                            }
                        } else {
                            console.warn("Login-Check impossible, maybe offline? In favour of the doubt, we'll keep you logged in.")
                            alert("Konnte keine Verbindung zum System herstellen. Bitte prüfe ob Du eine Internetverbindung hast und bestätige diesen Dialog für einen neuen Versuch.")
                            location.reload()
                        }
                        //reject(response)
                    }
                }
                window.jQuery.ajax(settings)
            }
        })
    }

    logout(redirect = null, message = null) {
        return new Promise(function (resolve, reject) {
            let apikey = window.jQuery.cookie("apikey")
            let result = false

            let postUrl = CONFIG.API_BASE_URL + "/logout"
            let realMessage = "Sie wurden abgemeldet."
            window.jQuery.ajax({
                type: "GET",
                url: postUrl,
                contentType: 'application/json;charset=UTF-8',
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                data: "",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('apikey', apikey)
                },
                success: function (response) {
                    window.jQuery.removeCookie("apikey", {path: '/'})
                    let realRedirect = CONFIG.LOGIN_URL
                    let timeout = 500
                    if (redirect == false) {
                        redirect = "#"
                        timeout = 60000000
                    }
                    if ((redirect != null && redirect != undefined) && redirect.length > 0) {
                        realRedirect = redirect
                    }

                    if (message != null) {
                        realMessage = message
                    }

                    if (typeof (response) != "undefined") {
                        window.jQuery.removeCookie("apikey", {path: '/'})
                        window.jQuery.removeCookie("consumer_id", {path: '/'})
                        window.jQuery.removeCookie("consumer_name", {path: '/'})
                        window.jQuery.removeCookie("username", {path: '/'})
                        if (!new PbAccount().silentMode) {
                            window.jQuery.blockUI({
                                blockMsgClass: 'alertBox',
                                message: realMessage,
                                timeout: timeout,
                                onUnblock: function () {
                                    $(location).attr('href', realRedirect)
                                }
                            })
                        }
                        // Fnon.Wait.Circle(realMessage, {
                        //     svgSize: { w: '50px', h: '50px' },
                        //     svgColor: '#3a22fa',
                        //     })
                        // Fnon.Wait.Remove(1000)
                        //     $(location).attr('href', realRedirect)
                    } else {
                        window.jQuery.removeCookie("apikey", {path: '/'})
                        if (!new PbAccount().silentMode) {
                            window.jQuery.blockUI({blockMsgClass: 'alertBox', message: realMessage})
                        }

                    }
                    resolve()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status != 401) {
                        console.warn("Login-Check impossible, maybe offline? In favour of the doubt, we'll keep you logged in.")
                        alert("Konnte keine Verbindung zum System herstellen. Bitte prüfe ob Du eine Internetverbindung hast und bestätige diesen Dialog für einen neuen Versuch.")
                        location.reload()
                    } else {
                        window.jQuery.removeCookie("apikey", {path: '/'})
                        if (!new PbAccount().silentMode) {
                            window.jQuery.blockUI({blockMsgClass: 'alertBox', message: realMessage})
                        }
                    }
                    reject()
                }
            })
        })
    }

    getapikey(doRedirect = true, successCallback = () => "", keepLogin = false, login_endpoint = "/login") {
        let self = this
        return new Promise(function (resolve, reject) {
            let apikey = window.jQuery.cookie("apikey")
            console.debug(apikey)
            let username = document.getElementById("login").value
            let password = document.getElementById("password").value
            Fnon.Box.Circle('form', '', {
                svgSize: {w: '50px', h: '50px'},
                svgColor: '#3a22fa',
            })
            //window.jQuery.blockUI({blockMsgClass: 'alertBox', message: 'Ihre Zugangsdaten werden überprüft ...'})
            let postUrl = CONFIG.API_BASE_URL + login_endpoint

            window.jQuery.ajax({
                type: "GET",
                url: postUrl,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                contentType: 'application/json;charset=UTF-8',
                data: "",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password))
                },
                success: function (response) {
                    if (typeof (response) != "undefined") {
                        //console.debug("LOGINRESPONSE", response)
                        if (!new PbAccount().silentMode) {
                            Fnon.Hint.Success('Anmeldung erfolgreich', {displayDuration: 3000})
                        }
                        let expires = undefined
                        if (keepLogin) {
                            expires = 365
                        }
                        window.jQuery.cookie("apikey", response.apikey, {expires: expires, path: '/'})
                        window.jQuery.cookie("consumer_name", response.consumer_name, {expires: expires, path: '/'})
                        window.jQuery.cookie("consumer_id", response.consumer.id, {expires: expires, path: '/'})
                        window.jQuery.cookie("username", response.username, {expires: expires, path: '/'})

                        new pbPermissions().getAbsoluteRightsOfRole("user." + username).then(
                            (response) => {
                                if (response?.rules) {
                                    PbAccount.instance.permissions = response.rules
                                }
                                if (keepLogin) {
                                    import("/csf-lib/vendor/fingerprint.js?v=[cs_version]").then(FingerprintJS => {
                                        FingerprintJS.load().then((fprint) => {
                                            fprint.get().then(result => {
                                                const visitorId = result.visitorId
                                                window.jQuery.cookie("visitorid", visitorId, {expires: expires, path: '/'})
                                                self.redirect(doRedirect, successCallback, response)
                                            })
                                        })
                                    }).catch(e => {
                                        window.jQuery.cookie("visitorid", "", {expires: expires, path: '/'})
                                        self.redirect(doRedirect, successCallback, response)
                                    })
                                } else {
                                    window.jQuery.cookie("visitorid", "", {expires: expires, path: '/'})
                                    self.redirect(doRedirect, successCallback, response)
                                }
                            })
                    } else {
                        window.jQuery.removeCookie("apikey", {path: '/'})
                        document.getElementById("login-error-msg").style.opacity = 0;
                        if (!new PbAccount().silentMode) {
                            Fnon.Hint.Info("Ihre Zugangsdaten werden überprüft ...", {
                                displayDuration: 1000, callback: function () {
                                    document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-danger error_message'>*Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut.*</div>"
                                    fadeIn("login-error-msg")
                                }
                            })
                        }

                        // window.jQuery.unblockUI()
                        // window.jQuery.blockUI({
                        //     blockMsgClass: 'alertBox',
                        //     message: 'Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut.',
                        //     timeout: 3000
                        // })
                        reject(response)
                    }
                },
                error: function (response) {
                    window.jQuery.removeCookie("apikey", {path: '/'})
                    if (!new PbAccount().silentMode) {
                        Fnon.Box.Remove('form', 500)
                        document.getElementById("login-error-msg").style.opacity = 0;
                        document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-outline-danger error_message'>*Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut.*</div>"
                        fadeIn("login-error-msg")
                    }

                    // window.jQuery.unblockUI()
                    // window.jQuery.blockUI({
                    //     blockMsgClass: 'alertBox',
                    //     message: 'Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut.',
                    //     timeout: 3000
                    // })
                    reject(response)
                }
            })
        })
    }

    redirect(doRedirect, successCallback, response) {
        if (doRedirect) {
            if (typeof doRedirect === 'string') {
                PbAccount.instance.redirectDialog(doRedirect)
            } else {
                PbAccount.instance.redirectDialog(CONFIG.DEFAULT_LOGIN_REDIRECT)
            }

        } else {
            Fnon.Box.Remove('form')
            if (!new PbAccount().silentMode) {
                Fnon.Hint.Success('Anmeldung erfolgreich', {displayDuration: 3000})
            }
        }
        successCallback(response?.apikey)
        //resolve(response)
    }

    getapikeyfor(username, password, callback) {
        return new Promise(function (resolve, reject) {
            let postUrl = CONFIG.API_BASE_URL + "/login"
            window.jQuery.ajax({
                type: "GET",
                url: postUrl,
                contentType: 'application/json;charset=UTF-8',
                data: "",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password))
                },
                success: function (response) {
                    //console.log("RESPONSE", response)
                    if (typeof (response) != "undefined") {
                        callback(response.apikey)
                        resolve(response)
                    } else {
                        callback()
                        resolve()
                    }
                },
                error: function (response) {
                    callback(false)
                    reject(response)
                }
            })
        })
    }

    redirectDialog(url = null) {
        if (url == null) {
            location.href = CONFIG.LOGIN_URL
        } else {
            location.href = url
        }
    }

    changePassword(username, password, onsuccess = null, onfailure = null) {

        Fnon.Box.Circle('form', '', {
            svgSize: {w: '50px', h: '50px'},
            svgColor: '#3a22fa',
        })

        return new Promise(function (resolve, reject) {
            let data = {
                "username": username,
                "password": password
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + "/update_credentials",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "apikey": $.cookie("apikey")
                },
                "data": JSON.stringify(data),
                "success": function () {
                    if (!new PbAccount().silentMode) {
                        Fnon.Hint.Success('Passwort wurde aktualisiert.', {displayDuration: 3000, callback: resolve})
                    }
                    if (onsuccess != null) {
                        onsuccess()
                        Fnon.Box.Remove('form')
                    } else {
                        setTimeout(function () {
                            Fnon.Box.Remove('form')
                        }, 2000)
                    }
                },
                "error": function () {
                    if (!new PbAccount().silentMode) {
                        Fnon.Hint.Danger('Passwort konnte nicht aktualisiert werden, bitte versuchen sie es später noch einmal.', {
                            displayDuration: 3000,
                            callback: reject
                        })
                    }
                    if (onsuccess != null) {
                        onfailure()
                        reject()
                        Fnon.Box.Remove('form')
                    } else {
                        setTimeout(function () {
                            Fnon.Box.Remove('form')
                        }, 2000)
                    }
                },
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
            });
        })
    }

    fetchAccountProfileData(account_id) {

        let url = CONFIG.API_BASE_URL + "/get_account_profile?account_id=" + account_id

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": url,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "error": reject
            };

            $.ajax(settings).done(function (response) {
                resolve(response)
            })
        })
    }

    setAccountProfileData(data) {
        PbAccount.instance.accountProfileData = data
    }

    getAccountProfileData() {
        return PbAccount.instance.accountProfileData
    }

    getFormattedBillingAddressBlock() {
        let data = this.getBillingAddress()
        let formatted = `${data?.firstname} ${data?.lastname}<br/>${data?.street} ${data?.number}<br/>${data?.zipcode}, ${data?.city} ${data?.country}`
        return formatted
    }

    hasValidBillingAddress() {
        let data = this.getBillingAddress()
        return data?.firstname!=undefined && data?.lastname!=undefined && data?.street!=undefined && data?.number!=undefined && data?.zipcode!=undefined && data?.city!=undefined && data?.country!=undefined
    }


    getBillingAddress() {
        return this.getAccountProfileData().addresses?.billing
    }

    saveAccountProfileData(account_id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateAccountProfileData",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({"account_id": account_id, "data": PbAccount.instance.accountProfileData}),
                "error": reject
            }

            $.ajax(settings).done(function (response) {
                resolve(response)
            })
        })
    }

    initListeners() {

    }

}
