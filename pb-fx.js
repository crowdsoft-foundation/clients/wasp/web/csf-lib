export function celebrateWithPapers(durationInSeconds = 0, numPapers = 50) {
    const container = document.createElement("div");
    container.classList.add("paper-container");
    document.body.appendChild(container);

    const colors = ["#FF5733", "#FFC300", "#DAF7A6", "#FF33F6", "#33C1FF", "#9CFF33", "#FF3333"];
    let stopGenerating = false;

    // Function to create and animate a single paper cut
    function createPaper() {
        if (stopGenerating) return;

        const paperWrapper = document.createElement("div");
        paperWrapper.classList.add("paperWrapper");
        container.appendChild(paperWrapper);

        const paper = document.createElement("div");
        paper.classList.add("paper");
        paperWrapper.appendChild(paper);

        // Set random position, color, and slight speed variation for turbulence
        paperWrapper.style.left = `${Math.random() * 100}vw`;
        paperWrapper.style.animationDuration = `${5 + (Math.random() - 0.5) * 2}s`; // Base fall speed ~5s
        paper.style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];

        // Randomize rotation direction and add a CSS class accordingly
        if (Math.random() > 0.5) {
            paper.classList.add("rotateClockwise");
        } else {
            paper.classList.add("rotateCounterClockwise");
        }

        // Remove paper after animation ends
        paperWrapper.addEventListener("animationend", () => paperWrapper.remove());
    }

    // Function to stagger paper creation within an interval
    function spawnPapers() {
        for (let i = 0; i < numPapers; i++) {
            setTimeout(() => {
                createPaper();
            }, Math.random() * 200); // Spread each spawn within 200 ms
        }
    }

    // Generate the papers at intervals, with staggered spawning
    const interval = setInterval(spawnPapers, 500);

    // Stop creating new papers if a duration is set and has passed
    if (durationInSeconds > 0) {
        setTimeout(() => {
            stopGenerating = true;
            clearInterval(interval);
        }, durationInSeconds * 1000);
    }

    // CSS styles
    const style = document.createElement("style");
    style.innerHTML = `
        .paper-container {
            position: fixed;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            pointer-events: none;
            overflow: hidden;
            z-index: 1000;
        }
        .paperWrapper {
            position: absolute;
            top: -5%;
            animation: fall linear forwards;
        }
        .paper {
            width: 15px;
            height: 25px;
            opacity: 0.8;
            border-radius: 3px;
        }
        .rotateClockwise {
            animation: rotateClockwise 5s linear infinite;
        }
        .rotateCounterClockwise {
            animation: rotateCounterClockwise 5s linear infinite;
        }
        @keyframes fall {
            0% { transform: translateY(-100px); opacity: 1; }
            100% { transform: translateY(100vh); opacity: 0; }
        }
        @keyframes rotateClockwise {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        @keyframes rotateCounterClockwise {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(-360deg); }
        }
    `;
    document.head.appendChild(style);
}