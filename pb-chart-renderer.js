import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {stringToHslColor, setQueryParam, md5} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"


export class PbChartRenderer {
    constructor(header_target_selector, chart_target_selector, rawdata) {
        this.chart_target_selector = chart_target_selector
        this.header_target_selector = header_target_selector
        this.rawdata = rawdata
        this.recordCount = 0
        this.config = {
            type: "bar",
            data: {
                labels: [],
                datasets: []
            },
            options: {
                indexAxis: 'y',
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Chart.js Horizontal Bar Chart'
                    }
                },
                scales: {
                    y: {
                        title: {
                            display: true,
                            text: ''
                        },
                        ticks: {
                            maxRotation: 0,
                            minRotation: 0
                        }
                    },
                    x: {
                        title: {
                            display: true,
                            text: ''
                        },
                        ticks: {
                            maxRotation: 0,
                            minRotation: 0
                        }
                    }
                }
            }
        }
    }

    setRecordCount(recordCount) {
        this.recordCount = recordCount
    }

    getRecordCount() {
        return this.recordCount
    }

    updateOptionsByDotPaths(target, updates) {
        // Create a deep copy of the original object
        const result = JSON.parse(JSON.stringify(target));

        for (const [dotPath, value] of Object.entries(updates)) {
            const keys = dotPath.split('.'); // Split the dot path into individual keys
            let current = result;

            // Traverse to the desired property location in the copied object
            for (let i = 0; i < keys.length - 1; i++) {
                const key = keys[i];
                if (!(key in current)) {
                    current[key] = {}; // Create the intermediate object if it doesn’t exist
                }
                current = current[key];
            }

            // Set the final key to the specified value
            current[keys[keys.length - 1]] = value;
        }

        return result; // Return the modified copy
    }

    setChartData(transformer, options = {}) {
        let data = transformer(this.rawdata)
        this.labels = data.labels
        this.datasets = []

        for (let i = 0; i < data.datasets.length; i++) {
            const color = stringToHslColor(data.datasets[i].label)
            const dataset = {
                label: data.datasets[i].label,
                data: data.datasets[i].data,
                backgroundColor: data.datasets[i].backgroundColor || color,
                borderColor: data.datasets[i].borderColor || color,
                borderWidth: data.datasets[i].borderWidth || 1
            }

            this.datasets.push(dataset)
        }

        this.config.data.labels = this.labels
        this.config.data.datasets = this.datasets

        return this
    }

    calculateChartHeight(labelCount, barHeight) {
        const padding = 50; // Zusätzlicher Platz für Titel und Achsentitel
        return labelCount * barHeight + padding;
    }


    getOptions() {
        return this.options
    }

    createCanvasNode(canvas_id, target_selector) {
        let targetNode = document.querySelector(target_selector);
        const col2 = document.createElement("div");
        col2.className = "canvas-design chart-box";

        const canvas = document.createElement("canvas");
        canvas.id = canvas_id
        col2.appendChild(canvas);
        targetNode.appendChild(col2);

        return canvas
    }

    delete() {
        $(this.chart_target_selector).remove()
    }

    async renderStatisticsHeader(numberOfRecords, start_date, end_date) {
        await PbTpl.instance.renderIntoAsync('snippets/statistics/header.html', {
            "total_entries": numberOfRecords, // Anzahl der Entries
            "start_date": start_date,
            "end_date": end_date,
        }, this.header_target_selector).then(function () {
            $(".datepicker").datetimepicker({
                format: CONFIG.DATEFORMAT,
                sideBySide: true,
                showClose: true,
                ignoreReadonly: true,
                debug: false,
                widgetPositioning: {horizontal: 'left', vertical: 'bottom'}
            });

            let startValue = moment(start_date, CONFIG.DB_DATEFORMAT_NOTIME)
            let endValue = moment(end_date, CONFIG.DB_DATEFORMAT_NOTIME)
            $("#start_date").datetimepicker('date', startValue)
            $("#end_date").datetimepicker('date', endValue)

            $("#show_statistic").off('click')
            $("#show_statistic").on('click', function (e) {
                setQueryParam("start_date", $("#start_date").datetimepicker('viewDate').format(CONFIG.DB_DATEFORMAT_NOTIME));
                setQueryParam("end_date", $("#end_date").datetimepicker('viewDate').format(CONFIG.DB_DATEFORMAT_NOTIME));
                $(this.header_target_selector).html("")
                $(this.chart_target_selector).html("")
                this.render();
            }.bind(this))
        }.bind(this));
    }


    renderDiagram(chart_id, options = {}) {
        const ctx = this.createCanvasNode(chart_id, this.chart_target_selector).getContext('2d')
        // Set the canvas height dynamically based on the number of labels
        // Assuming each label should have a height of 30px, you can adjust this value as needed
        ctx.height = this.labels.length * 30;
        
        let config = this.updateOptionsByDotPaths(this.config, options)
        new Chart(ctx, config)

        return this
    }
}
