import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import "/csf-lib/vendor/mozilla/nunjucks.min.js?v=[cs_version]"

export class PbTpl {
    constructor(nunjuckOptions = undefined) {
        if (!nunjuckOptions) {
            nunjuckOptions = {autoescape: true, web: {useCache: true, async: true}}
        }
        if (!PbTpl.instance) {
            this.i18n = new pbI18n()
            PbTpl.instance = this
            PbTpl.instance.nunjuckOptions = nunjuckOptions

        }
        return PbTpl.instance
    }

    translate(vocab) {
        return this.i18n.translate(vocab)
    }

    init() {
        PbTpl.instance.nunjucks = nunjucks.configure(CONFIG.APP_BASE_URL, PbTpl.instance.nunjuckOptions)
        PbTpl.instance.nunjucks.addFilter('date', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.DATETIMEFORMAT)
        })

        PbTpl.instance.nunjucks.addFilter('is_array', function (obj) {
            return Array.isArray(obj);
        });

        PbTpl.instance.nunjucks.addFilter('datenotime', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.DATEFORMAT)
        })

        PbTpl.instance.nunjucks.addFilter('time', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.TIMEDISPLAYFORMAT)
        })

        PbTpl.instance.nunjucks.addFilter('weekday', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.DATEFORMAT_WEEKDAY)
        })

        PbTpl.instance.nunjucks.addFilter('mergeArrays', function (arr1, arr2) {
            const mergedArray = arr1.concat(arr2);
            const uniqueArray = [...new Set(mergedArray.map(item => item))];
            uniqueArray.sort((a, b) => a.localeCompare(b));
            return uniqueArray;
        });


        PbTpl.instance.nunjucks.addFilter('tojson', function (obj) {
            return JSON.stringify(obj);
        });

        PbTpl.instance.nunjucks.addFilter('map', function (arr, prop) {
            return arr.map(function (item) {
                const props = prop.split('.');
                let value = item;
                props.forEach(function (p) {
                    if (value && value[p] !== undefined) {
                        value = value[p];
                    } else {
                        value = undefined;
                    }
                });
                return value;
            });
        });

        PbTpl.instance.nunjucks.addFilter('split', function (str, separator) {
            if (typeof str === 'string' && separator) {
                return str.split(separator);
            } else {
                return [];
            }
        });

        PbTpl.instance.nunjucks.addFilter('nl2br', function (str) {
            if (typeof str === 'string') {
                return str.replace(/\n/g, '<br>');
            }
            return str;
        });


        return PbTpl.instance
    }

    renderAsync(tpl, params, callback = () => "") {
        let data = PbTpl.instance.nunjucks.render(tpl, params, function (err, res) {
            if (err) {
                console.error('pb-tpl reports: Error rendering parent template:', err);
            }
            callback(err, res)
        })
        return data
    }

    render(tpl, params) {
        let data = PbTpl.instance.nunjucks.render(tpl, params)
        return data
    }

    renderIntoAsync(tpl, params, targetSelector, behaviour = "overwrite") {
        return new Promise(function (resolve, reject) {
            PbTpl.instance.renderAsync(tpl, params, function (err, body) {
                let data = body
                if (behaviour == "append") {
                    data = $(targetSelector).append(data)
                }
                if (behaviour == "prepend") {
                    data = $(targetSelector).prepend($(data))
                }
                if (behaviour == "overwrite") {
                    $(targetSelector).html(data)
                }

                new PbAccount().handlePermissionAttributes()
                new pbI18n().replaceI18n()
                resolve()
            })
        })
    }

    showStickyBanner(text) {

        var banner = document.getElementById("stickyBanner") || document.createElement("div")
        banner.id = "stickyBanner"
        banner.textContent = text
        document.body.appendChild(banner)
        $("#stickyBanner").show()
    }

    removeStickyBanner() {
        $("#stickyBanner").hide()
    }

    renderInto(tpl, params, targetSelector, behaviour = "overwrite") {
        PbTpl.instance.nunjucks = nunjucks.configure(CONFIG.APP_BASE_URL, {
            autoescape: true,
            web: {useCache: true, async: false}
        })
        PbTpl.instance.nunjucks.addFilter('date', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.DATETIMEFORMAT)
        })
        PbTpl.instance.nunjucks.addFilter('is_array', function (obj) {
            return Array.isArray(obj);
        });

        PbTpl.instance.nunjucks.addFilter('datenotime', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.DATEFORMAT)
        })

        PbTpl.instance.nunjucks.addFilter('time', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.TIMEDISPLAYFORMAT)
        })

        PbTpl.instance.nunjucks.addFilter('weekday', (data) => {
            let createtime = moment(data)
            return createtime.format(CONFIG.DATEFORMAT_WEEKDAY)
        })

        let data = PbTpl.instance.render(tpl, params)
        if (behaviour == "append") {
            data = $(targetSelector).append(data)
        }
        if (behaviour == "prepend") {
            data = $(targetSelector).prepend($(data))
        }
        if (behaviour == "overwrite") {
            $(targetSelector).html(data)
        }
        new PbAccount().handlePermissionAttributes()
        new pbI18n().replaceI18n()
        PbTpl.instance.nunjucks = nunjucks.configure(CONFIG.APP_BASE_URL, PbTpl.instance.nunjuckOptions)
    }
}