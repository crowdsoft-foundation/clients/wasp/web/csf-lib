import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {getCookie} from "./pb-functions.js?v=[cs_version]";

export class QrCodeLinker {
    constructor(name, value, socket, guest_login = false, type = "url", error_correct = "H", kind = "default", logo_url = "") {
        this.socket = socket
        this.name = name
        // For now only url is supportede
        this.type = "url"
        this.guest_login = guest_login
        this.value = value
        // Can be
        // "L" (7% Correction_level)
        // "M" (15% Correction_level)
        // "Q" (25% Correction_level)
        // "H" (30% Correction_level)
        // Use H if a logo shall be included in the QRCode, otherwise the qrcode is likely not readable
        this.error_correct = error_correct
        // Kind can be "default" or "logo".
        // If logo is selected, logo_url has to be set to a an image url which is public available.
        this.kind = kind
        this.logo_url = logo_url
        this.updatePayload()
        this.initListeners()
        this.qrCodeCreatedMessages = {}
        this.qrCodeCreatedMessagesHandlers = {}
    }

    updatePayload() {
        this.payload = {
            "type": this.type,
            "guest_login": this.guest_login,
            "value": this.value
        }
        return this.payload
    }

    createQRCode() {
        let payload = {
            "name": this.name,
            "data": {
                "payload": this.updatePayload(),
                "error_correct": this.error_correct,
                "kind": this.kind,
                "logo_url": this.logo_url
            }
        }
        let thisObject = this
        return new Promise(function (resolve, reject) {
            let settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createQRCode",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    thisObject.qrCodeCreatedMessagesHandlers[response.task_id] = function() {}
                    if(thisObject.qrCodeCreatedMessages[response.task_id]) {
                        thisObject.qrCodeCreatedMessagesHandlers[response.task_id]()
                    }
                    resolve(thisObject)
                },
                "error": (response) => {
                    alert("Error");
                    reject(thisObject)
                }
            }

            $.ajax(settings)
        })
    }

    initListeners() {
        let thisObject = this
        if (thisObject.socket) {
            thisObject.socket.commandhandlers["qrCodeCreated"] = function (args) {
                let url = CONFIG.API_BASE_URL + "/sqr/" + args.qr_code_id
                $("#sqr_url").html(url)
                if(thisObject.qrCodeCreatedMessagesHandlers[args.task_id]) {
                    thisObject.qrCodeCreatedMessagesHandlers[args.task_id]()
                }
                thisObject.qrCodeCreatedMessages[args.task_id] = args

            }
        }
    }
}





