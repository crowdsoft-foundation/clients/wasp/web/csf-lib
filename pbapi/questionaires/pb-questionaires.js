import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbQuestionaires {
    constructor() {}

    fetchQuesstionaireConfigById(config_id, withMeta=true) {
        let meta = withMeta ? 'true' : 'false';
        return new Promise(async (resolve, reject) => {
            new PbApiRequest(`/get_questionaire_config_by_id/${config_id}?with_meta=${meta}`).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    };

    saveAnswers(answers) {
        return new Promise(async (resolve, reject) => {
            let payload = {
                "data": answers,
                "data_owners": []
            }
            new PbApiRequest(`/es/cmd/addQuestionaireAnswers`, "POST", payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    }
}
