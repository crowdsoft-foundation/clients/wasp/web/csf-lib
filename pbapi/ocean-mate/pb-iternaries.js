import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"
import {CONFIG} from "../../../pb-config.js?v=[cs_version]";
import {getCookie} from "../../pb-functions.js?v=[cs_version]";


export class PbIternaries {
    constructor() {
        this.data = {};
        this.newItinerary = {};
    }

    loadFromApi() {
        let promise = new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/ocean-mate/iternaries?show_hidden=true",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie('apikey')
                },

                "success": function (response) {
                    this.data = response;
                    resolve(response)
                }.bind(this),
                "error": function (error) {
                    reject(error);
                }
            };

            $.ajax(settings)
        }.bind(this))

        return promise
    }

    setActiveIternary(id) {
        this.activeIternary = this.data[id];
    }

    getActiveIternary() {
        return this.activeIternary;
    }

    updateActiveIternary() {
        let promise = new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateIternary",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie('apikey'),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "id": this.activeIternary.id,
                    "update_behaviour": this.activeIternary.update_behaviour || "ignore",
                    "data": this.activeIternary
                }),
                "success": function (response) {
                    resolve()
                }.bind(this),
                "error": function (error) {
                    reject(error);
                }
            };

            $.ajax(settings)
        }.bind(this))

        return promise
    }

    createBlankItinerary() {
        this.activeIternary = {
                "is_auto_update": false,
                "departures": [
                    {
                        "cabins": [
                            {
                                "availableCabins": 0,
                                "category": "",
                                "doubleOccupancyRate": 0,
                                "passengerType": "",
                                "singleOccupancyRate": 0,
                                "superCategory": ""
                            }
                        ],
                        "embarkationDate": "",
                        "endDate": "",
                        "shipCode": "",
                        "startDate": ""
                    }
                ],
                "description": "",
                "from_port": "",
                "id": "",
                "intro": "",
                "preview_image_urls": [],
                "route_stations": [],
                "ship": {
                    "id": 1,
                    "name": ""
                },
                "shipping_line": {
                    "id": 1,
                    "name": ""
                },
                "title": "",
                "to_port": "",
                "tags": [],
                "hide": false
            }
    }
}