import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"
import {getCookie, makeId} from "/csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"

export class PbMessages {
    static baseconfig = {
        config: {apikey: getCookie("apikey"), api_base_url: CONFIG.API_BASE_URL},
        onBackendUpdateFunc: () => "",
        onCommandError: () => "",
        onEventError: () => "",
    }


    constructor(args = PbMessages.baseconfig) {
        this.messageList = [];
        this.templateList = {};
        this.activeTemplate = {};
        this.config = args.config
        this.onBackendUpdateFunc = args.onBackendUpdateFunc
        this.onCommandError = args.onCommandError
        this.onEventError = args.onEventError
        this.allConfigs = undefined;
        return this
    }

    fetchMessageList() {
        return new Promise(async (resolve, reject) => {
            new PbApiRequest("/messages/list").execute().then(function (response) {
                this.messageList = response;
                resolve(response);
            }.bind(this));
        });
    };

    putMessage(payload) {
        return new Promise(async (resolve, reject) => {
            new PbApiRequest("/es/cmd/putMessageCenterMessage", 'POST', payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    }

    deleteMessage(id, version) {
        return new Promise(async (resolve, reject) => {
            let payload = {
                "id": id,
                "version": version
            };

            new PbApiRequest("/es/cmd/deleteMessageCenterMessage", 'POST', payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    };

    fetchEmailConfig() {
        return new Promise(async (resolve, reject) => {
            new PbApiRequest("/messages/email_server_config").execute().then(function (response) {
                resolve(response);
            }.bind(this)).catch(function (response) {
                // Special Case: If there is no entry in the database, we will create a default entry.
                if (response.status === 444) {
                    resolve(response.responseJSON)
                }
            });
        });
    };

    putEmailConfig(payload) {
        return new Promise(async(resolve, reject) => {
            new PbApiRequest("/es/cmd/putMessageCenterMailServerData", 'POST', payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    };

    // ToDo: Perhaps we use "type" (event.detail.configType) to fetch other templates instead of MAIL only
    fetchTemplatesData(type = undefined) {
        return new Promise(async function (resolve, reject)  {
            new PbApiRequest("/messages/mail_templates").execute().then(function (response) {

                resolve(response);
            }.bind(this))
        }.bind(this));
    }

    async putMessageTemplate(template) {
        template["id"] = template.meta_data.id;
        template["data_owners"] = template.meta_data.data_owners;
        template["version"] = template.meta_data.version;

        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": `${this.config.api_base_url}/es/cmd/putMessageCenterTemplate`,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.config.apikey,
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify(template),
                    "success": async function (response) {

                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            console.log('error', e)
        }
    }

    fetchQuickRepliesData() {
        return new Promise(async function (resolve, reject)  {
            new PbApiRequest("/messages/quickreplys/list").execute().then(function (response) {
                resolve(response);
            }.bind(this))
        }.bind(this));
    }

    putQuickReplyTemplate(payload) {
        return new Promise(async(resolve, reject) => {
            new PbApiRequest("/es/cmd/putMessageCenterQuickReply", 'POST', payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    }
}
