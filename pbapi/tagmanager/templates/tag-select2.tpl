<select id="tags" disabled data-propertyname="{{ propertyPath }}" data-select2-selector="" class="select-tags form-control" {% if multiple %}multiple="multiple" {% endif %} style="width: 100%;">
    {% for option in options %}
    <option value="{{ option.id }}" {{ option.selected }} data-i18n={{ option.name}}>{{ option.name }}</option>
    {% endfor %}
</select>
{% if addButton %}
<button data-permission="tagmanager.globaltags.add" title="Add Global Tag" class="btn btn-indigo btn-with-icon no-wrap ml-2 mb-0 rounded hidden" type="button" id="addGlobalTagButton">
    <i class="typcn typcn-plus-outline"></i>
</button>
{% endif %}