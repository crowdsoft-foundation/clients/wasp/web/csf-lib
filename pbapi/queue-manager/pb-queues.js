import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbQueues {

    constructor() {
        this.filter = {"filters": []}
        this.resources = {}
        this.queues = []
        this.data = []
        if (!PbQueues.instance) {
            PbQueues.instance = this
        }

        return PbQueues.instance
    }

    fetchRessources() {
        return new Promise((resolve, reject) => {
            new PbApiRequest("/get_taskmanager_resources").execute().then(function (response) {
                PbQueues.instance.resources = response
                resolve(response)
            })
        });
    }

    fetchQueueLists() {
        return new Promise((resolve, reject) => {
            new PbApiRequest("/queues/list").execute().then(function (response) {
                PbQueues.instance.queues = response.queues
                resolve(response.queues)
            })
        });
    }

    search(filter = {"filters": []}, force_reload = false) {
        // Check if the filter is the same as the previous one
        let no_filter_changes = JSON.stringify(PbQueues.instance.filter) == JSON.stringify(filter)

        return new Promise((resolve, reject) => {
            if (no_filter_changes && !force_reload && PbQueues.instance.data.length > 0) {
                resolve(PbQueues.instance.data)
            } else {
                PbQueues.instance.filter = filter
                new PbApiRequest("/tasks/search", "POST", PbQueues.instance.filter).execute().then(function (response) {
                    PbQueues.instance.previous_data = PbQueues.instance.data
                    PbQueues.instance.data = response.tasks
                    resolve(response.tasks)
                })
            }
        });
    }

    updateQueueEntry(entry) {
        return new Promise((resolve, reject) => {
            new PbApiRequest("/es/cmd/updateQueueEntry", "POST", entry).execute().then(function (response) {
                resolve(response)
            })
        });
    }

    newQueueEntry(entry) {
        return new Promise((resolve, reject) => {
            new PbApiRequest("/es/cmd/createNewQueueEntry", "POST", entry).execute().then(function (response) {
                resolve(response)
            })
        });
    }
}
