import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbEventTypes {
    #data = []

    constructor() {
        if (!PbEventTypes.instance) {
            PbEventTypes.instance = this
        }

        return PbEventTypes.instance
    }

    async loadFromApi() {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest("/cb/geteventtypes").execute().then((response) => {
                self.#data = response
                resolve(self)
            })
        })
    }

    async set(payload) {
        return new Promise((resolve, reject) => {
            new PbApiRequest("/es/cmd/setAppointmentType", "POST", payload).execute().then(() => {
                resolve()
            }).catch(()=>reject)
        })
    }

    getData() {
        return this.#data
    }

    getSelectOptions(selectedIds= []) {
        let type_options = []

        for (let entry of this.#data) {
            let option = {
                "id": entry.data.type_id,
                "name": entry.data.type_name,
                "selected": (selectedIds.length > 0 && selectedIds.includes(entry.data.type_id)) || selectedIds.length == 0 && entry.data.default ? "selected" : ""
            }
            type_options.push(option)
        }
        return type_options
    }

    async deleteByUUId(uuid) {
        return new Promise(function (resolve, reject) {
            let payload = {
                "data": {
                    "uuid": uuid
                }
            }
            new PbApiRequest("/es/cmd/removeAppointmentType", "POST", payload).execute().then(async function (response) {
                resolve()
            }.bind(this))
        }.bind(this))
    }
}
