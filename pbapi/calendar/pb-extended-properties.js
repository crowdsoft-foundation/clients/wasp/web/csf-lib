import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbExtendedProperties {
    #data = {}

    constructor() {

    }

    loadFromApi() {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest("/cb/extended_properties").execute().then((response) => {
                self.#data = response
                resolve(self)
            })
        })
    }

    getData() {
        return this.#data
    }
}
