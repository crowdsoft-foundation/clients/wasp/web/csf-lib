import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbEventReminders {
    #data = {}
    #event_id = undefined
    #lastTaskId
    #onChanges
    #socket

    constructor(socket, onChanges = () => "") {
        this.#socket = socket
        this.#onChanges = onChanges
        this.initBackendListeners()
    }

    loadFromApi(event_id = undefined) {
        this.#event_id = event_id
        let self = this
        return new Promise((resolve, reject) => {
            if (self.#event_id) {
                new PbApiRequest("/cb/my_event_reminders/" + self.#event_id).execute().then((response) => {
                    self.#data = response
                    resolve(self)
                })
            } else {
                resolve(self)
            }
        })
    }

    initBackendListeners() {
        let self = this
        document.addEventListener("notification", function _(event) {
            if (event.data.data.event == "easy2scheduleReminderDeleted") {
                self.#lastTaskId = undefined
                self.loadFromApi(self.#event_id).then(() => self.#onChanges())
            }

            if (event.data.data.event == "easy2scheduleReminderSet") {
                self.#lastTaskId = undefined
                self.loadFromApi(self.#event_id).then(() => self.#onChanges())
            }
        })
    }

    deleteFromApi(reminderId) {
        let self = this
        return new Promise((resolve, reject) => {
            if (reminderId) {
                new PbApiRequest("/es/cmd/easy2scheduleDeleteReminder", "POST", {"reminder_id": reminderId}).execute().then((response) => {
                    self.#lastTaskId = response.task_id
                    resolve(self)
                })
            } else {
                resolve(self)
            }
        })
    }

    getData() {
        return this.#data
    }
}
