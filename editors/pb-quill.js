import "/style-global/lib/quill/quill.min.js?v=[cs_version]"

export class PbQuill {
    constructor(targetSelector, options = undefined) {
        let targetElement = document.querySelector(targetSelector);
        if (!targetElement) {
            throw "No element for this targetSelector found"
        } else {
            this.targetSelector = targetSelector
        }

        this.options = {
            modules: {
                toolbar: [
                    [{'header': [1, 2, 3, 4, 5, 6, false]}],
                    ['bold', 'italic', 'underline'],
                    [{'align': ''}, {'align': 'center'}, {'align': 'right'}],
                    [{'list': 'ordered'}, {'list': 'bullet'}],
                    ['link'],
                    ['clean']
                ]
            },
            placeholder: 'Begin ein neues Dokument, indem du hier schreibst ...',
            theme: 'snow'
        }
        if (options)
            this.options = Object.assign(this.options, options)

        return this
    }

    init() {
        return new Promise((resolve, reject) => {
            let self = this
            $('<link/>', {
                rel: 'stylesheet',
                type: 'text/css',
                href: '/style-global/lib/quill/quill.snow.css?v=[cs_version]'
            }).appendTo('head');
            self.raw_editor = new Quill(self.targetSelector, self.options)
            resolve(self)
        })
    }

    setContent(content) {
        this.raw_editor.clipboard.dangerouslyPasteHTML(content)
        return this
    }

    getContent() {
        return this.raw_editor.root.innerHTML
    }

    onTextChanged(func) {
        this.raw_editor.on('text-change', func)
    }
}