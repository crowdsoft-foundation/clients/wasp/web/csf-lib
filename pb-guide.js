import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {getCookie, setCookie, getQueryParam, removeParamFromAddressBar} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {PbLogin} from "../../csf-lib/pb-login.js?v=[cs_version]"

export class PbGuide {
    constructor(tourFile, options = {}, onBeforeTour = (PbGuide) => "", onBeforeExit = (PbGuide) => "", onComplete = (PbGuide) => "", forceReload = false) {
        if (!PbGuide.instance) {
            PbGuide.instance = this
            PbGuide.instance.tourFile = tourFile
            PbGuide.instance.options = options
            PbGuide.instance.tourConfig = undefined
            PbGuide.instance.onBeforeTour = onBeforeTour
            PbGuide.instance.onBeforeExit = onBeforeExit
            PbGuide.instance.onComplete = onComplete
            PbGuide.instance.forceReload = forceReload
            $(document).on('click', '#help', function (e) {
                PbGuide.instance.init().then((guide) => {
                    guide.loadTourFile()
                })
            })

            if (getQueryParam("tour")) {
                PbGuide.instance.startTourFromQueryParam(getQueryParam("tour"))
                removeParamFromAddressBar("tour")
            }
            nunjucks.configure({autoescape: true});

        }
        return PbGuide.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            if (typeof introJs != "undefined") {
                PbGuide.instance.introguide = introJs()
                resolve(PbGuide.instance)
            } else {
                $('<link/>', {
                    rel: 'stylesheet',
                    type: 'text/css',
                    href: '/style-global/lib/introjs/introjs.min.css?v=[cs_version]'
                }).appendTo('head');

                let file = {
                    name: "introjs",
                    path: "/style-global/lib/introjs/intro.min.js?v=[cs_version]",
                    loaded: null
                }
                window.jQuery.getScript(file.path)
                    .done(function (script, textStatus) {
                        PbGuide.instance.introguide = introJs()
                        resolve(PbGuide.instance)
                    })
                    .fail(function (jqxhr, settings, exception) {
                        reject()
                    })
            }
        })
    }

    ensureUrl(pathname, tourname) {
        let tour = `?tour=${tourname}`
        let shortPath = undefined //to deal with short url if index page is wanted
        let newURL = pathname + tour

        if (pathname.includes("index")) {
            shortPath = pathname.replace('/index.html', '/')
        }

        if (!location.pathname.includes(pathname) && shortPath) { //path is not found in current href
            //alert("short path is " + shortPath)
            if (location.pathname != shortPath) {
                window.location.assign(newURL)
                return false
            }
        }

        if (!location.pathname.includes(pathname) && !shortPath) { //path is not found in current href
            window.location.assign(newURL)
            return false
        }

        return true

    }

    hasPermission(permission) {
        return new PbAccount().hasPermission(permission) 
    }

    loadTourFile(tour_file = PbGuide.instance.tourFile, callback = () => "") {
        let file = tour_file + "?v=[cs_version]"
        window.jQuery.getScript(file)
            .done(function (script, textStatus) {
                for (let [tour_id, tour_data] of Object.entries(window.tour_config.tours)) {
                    if (tour_data.permission && tour_data.permission.length > 0 && !new PbAccount().hasPermission(tour_data.permission)) {
                        delete window.tour_config.tours[tour_id]
                    }

                }
                PbGuide.instance.tourConfig = window.tour_config

                PbGuide.instance.setHelpModalTopics()
                delete window.tour_config

                callback()
            })
            .fail(function (jqxhr, settings, exception) {
                alert("failed loading intro-tour")
            })
    }

    startTourFromQueryParam(tour) {
        PbGuide.instance.init().then(function () {
            PbGuide.instance.loadTourFile(PbGuide.instance.tourFile, function () {
                if (tour && PbGuide.instance.tourConfig.tours[tour]) {
                    PbGuide.instance.start(PbGuide.instance.tourConfig.tours[tour].startStep)
                }
            })
        })
    }

    setHelpModalTopics() {
        nunjucks.configure(CONFIG.APP_BASE_URL, {autoescape: false})
        let data = nunjucks.render('snippets/guide/modal-body.tpl', {tour_config: PbGuide.instance.tourConfig})
        $("#guide_modal_body").html(data)
        $(document).on('click', "div[data-tour]", function (e) {
            e.preventDefault()
            if (PbGuide.instance.ensureUrl(PbGuide.instance.tourConfig.tours[e.currentTarget.id].startPage, e.currentTarget.id)) {
                PbGuide.instance.start(PbGuide.instance.tourConfig.tours[e.currentTarget.id].startStep)
            }
        })
    }

    start(startStep = 0, onBeforeTour = PbGuide.instance.onBeforeTour, onBeforeExit = PbGuide.instance.onBeforeExit, onComplete = PbGuide.instance.onComplete, options = PbGuide.instance.options, forceReload = PbGuide.instance.forceReload) {
        PbGuide.instance.startTour(PbGuide.instance.tourConfig.tourSteps, startStep, onBeforeTour, onBeforeExit, onComplete, options)
    }

    startTour(steps, startStep = 0, onBeforeTour = PbGuide.instance.onBeforeTour, onBeforeExit = PbGuide.instance.onBeforeExit, onComplete = PbGuide.instance.onComplete, options = PbGuide.instance.options) {
        if (onBeforeTour) {
            onBeforeTour(PbGuide.instance)
        }

        // let tour_steps = steps.filter(function (obj) {
        //     //filter elements hidden by permissions
        //     return $(obj.element).is(":hidden") === false || obj.element == ".introjsFloatingElement"
        // })
        //Removing steps is not a good idea, since it messes up the order of steps
        let tour_steps = steps

        PbGuide.instance.introguide = introJs()

        let default_options = {
            showBullets: false,
            nextLabel: ">",
            prevLabel: "<",
            showProgress: false,
            doneLabel: pbI18n.instance.translate("Fertig"),
            exitOnOverlayClick: false,
            disableInteraction: false,
            keyboardNavigation: false,
            tooltipClass: 'customTooltip',
            showStepNumbers: false,
            steps: tour_steps,
        }

        PbGuide.instance.options = Object.assign({}, default_options, options);

        PbGuide.instance.introguide.setOptions(PbGuide.instance.options)

        PbGuide.instance.introguide.goToStepNumber(startStep).start()

        if (PbGuide.instance.introguide._introItems[startStep-1].myBeforeChangeFunction) {
            PbGuide.instance.introguide._introItems[startStep-1].myBeforeChangeFunction(PbGuide.instance)
        }

        if (PbGuide.instance.introguide._introItems[startStep-1].myChangeFunction) {
            PbGuide.instance.introguide._introItems[startStep-1].myChangeFunction(PbGuide.instance);
        }

        if (PbGuide.instance.introguide._introItems[startStep-1].myAfterChangeFunction) {
            PbGuide.instance.introguide._introItems[startStep-1].myAfterChangeFunction(PbGuide.instance);
        }


        PbGuide.instance.introguide.onbeforechange(function () {
            if(this._introItems[this._currentStep] && $(this._introItems[this._currentStep].element).is(":hidden")) {
                PbGuide.instance.introguide.nextStep()
                return false
            }
            PbGuide.instance.setPrevButton()
            PbGuide.instance.setNextButton()
            PbGuide.instance.showNavButtons()

            if (this._introItems[this._currentStep] && this._introItems[this._currentStep].myBeforeChangeFunction) {
                this._introItems[this._currentStep].myBeforeChangeFunction(PbGuide.instance)
            }

        })

        PbGuide.instance.introguide.onchange(function () {
            if(this._introItems[this._currentStep] && this._introItems[this._currentStep].myChangeFunction) {
                this._introItems[this._currentStep].myChangeFunction(PbGuide.instance);
            }
        })

        PbGuide.instance.introguide.onafterchange(function () {
            if (this._introItems[this._currentStep] && this._introItems[this._currentStep].myAfterChangeFunction) {
                this._introItems[this._currentStep].myAfterChangeFunction(PbGuide.instance);
            }
        })

        PbGuide.instance.introguide.onbeforeexit(function () {
            if (onBeforeExit) {
                onBeforeExit(PbGuide.instance)
            }

            introJs().addHints().setOptions({
                hintButtonLabel: pbI18n.instance.translate("Alles klar")
            }).hideHints().showHint(0).onhintclose(function () {
                $('#modal_help').modal('toggle');
            });
        })

        PbGuide.instance.introguide.oncomplete(function () {
            if (onComplete) {
                onComplete(PbGuide.instance)
            }
        })
    }

    setPrevButton(title = PbGuide.instance.options.prevLabel, onclick = () => "") {
        let prevButton = document.querySelector('.introjs-button.introjs-prevbutton')
        if (prevButton) {
            prevButton.innerHTML = title
            prevButton.addEventListener('click', e => {
                    e.preventDefault();
                    onclick(e)
                },
                {once: true}
            )
        }
    }

    setNextButton(title = PbGuide.instance.options.nextLabel, onclick = () => "") {
        PbGuide.instance.introguide.setOption("nextLabel", title)
        let nextButton = document.querySelector('.introjs-button.introjs-nextbutton')
        if (nextButton) {
            nextButton.innerHTML = title
            nextButton.addEventListener('click', e => {
                    e.preventDefault();
                    onclick(e)
                },
                {once: true}
            )
        }
    }

    hideNextButton() {
        let intro_next_button = document.querySelectorAll('.introjs-button.introjs-nextbutton')
        intro_next_button.forEach((el) => {
            el.style.display = "none";
        });
    }

    updateIntro(text) {
        let intro_tool_text = document.querySelectorAll('.introjs-tooltiptext')
        setTimeout(function(){
            intro_tool_text.forEach((el) => {
                el.innerHTML = text;
            });
        }, 500); 
        
    }

    showNextButton() {
        let intro_next_button = document.querySelectorAll('.introjs-button.introjs-nextbutton')
        intro_next_button.forEach((el) => {
            el.style.display = "initial";
        });
    }

    hidePreviousButton() {
        let intro_previous_button = document.querySelectorAll('.introjs-button.introjs-prevbutton')
        intro_previous_button.forEach((el) => {
            el.style.display = "none";
        });
    }

    showPreviousButton() {
        let intro_previous_button = document.querySelectorAll('.introjs-button.introjs-prevbutton')
        intro_previous_button.forEach((el) => {
            el.style.display = "initial";
        });
    }

    showNavButtons() {
        PbGuide.instance.showPreviousButton()
        PbGuide.instance.showNextButton()
    }

    hideNavButtons() {
        PbGuide.instance.hidePreviousButton()
        PbGuide.instance.hideNextButton()
    }

    markTourSeen(tour_name) {
        tour_name = tour_name.replace(".json", "")
        if (!PbGuide.instance.wasTourSeen(tour_name)) {
            let data = {}
            data[tour_name] = true
            PbLogin.instance.profileData["tours_seen"] = data
            PbLogin.instance.saveProfileData()
        }
    }

    wasTourSeen(tour_name) {
        tour_name = tour_name.replace(".json", "")
        if(PbLogin.instance?.profileData?.tours_seen?.[tour_name]) {
            return true
        } else {
            return false
        }
        // let tours_seen = getCookie("toursSeen")
        // if (tours_seen) {
        //     return tours_seen.includes(tour_name)
        // } else {
        //     return false
        // }
    }
}