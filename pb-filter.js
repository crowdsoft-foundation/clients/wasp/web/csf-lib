export class PbFilter {
    constructor(data, filters = {}) {
        this.filters = {};
        this.active_filters = [];
        this.inactive_filters = [];
    }

    addFilter(name, filter) {
        this.filters[name] = filter
    }

    removeFilter(name) {
        if (this.filters[name]) {
            delete this.filters[name];
        } else {
            console.warn(`Filter "${name}" does not exist.`);
        }
    }

    getFilter(filter_name) {
        return this.filters[filter_name]
    }

    setFilter(filter, returnOnly = false) {
        if (!this[filter.property_name + "_backup"]) {
            this[filter.property_name + "_backup"] = JSON.parse(JSON.stringify(this[filter.property_name]));

        }
        this.active_filters.push(filter)

        let sort_field_path = filter.sort_field.split('.');
        let filtered_data = this[filter.property_name].filter((task) => {
            let dotpathed_properties = Array.isArray(filter.field) ? filter.field : [filter.field];
            let concatenatedValues = '';

            dotpathed_properties.forEach((dotpathed_property) => {
                let value = task;
                let properties = dotpathed_property.split('.');
                properties.forEach((property) => {
                    value = value[property];
                });
                concatenatedValues += value; // Concatenate the value
            });

            let value = concatenatedValues
            if (filter.ignore_case && typeof value === 'string') {
                value = value.toLowerCase();
            }

            let localOffset = new Date().getTimezoneOffset();
            let storedDate = new Date(value)
            let adjustedDate = storedDate//new Date(storedDate.getTime() + localOffset * 60 * 1000);
            switch (filter.operator) {
                case "in":
                    if (filter.ignore_case && Array.isArray(filter.value)) {
                        return filter.value.map(v => typeof v === 'string' ? v.toLowerCase() : v).includes(value);
                    }
                    return filter.value.includes(value);
                    break;
                case "like":
                    if (filter.ignore_case && typeof filter.value === 'string') {
                        return value.includes(filter.value.toLowerCase());
                    }
                    return value.includes(filter.value);
                    break;
                case "after":
                    return filter.value < adjustedDate;
                    break;
                case "before":
                    return filter.value > adjustedDate;
                    break;
                case "eq":
                    return filter.value == value;
                    break;
            }
        });

        // Sorting logic
        filtered_data.sort((a, b) => {
            let a_value = a;
            let b_value = b;
            sort_field_path.forEach((property) => {
                a_value = a_value[property];
                b_value = b_value[property];
            });

            // Convert to string and to lowercase for case-insensitive comparison if ignore_case is true
            if (filter.ignore_case) {
                a_value = String(a_value).toLowerCase();
                b_value = String(b_value).toLowerCase();
            } else {
                a_value = String(a_value);
                b_value = String(b_value);
            }

            if (filter.sort_order === 'asc') {
                return (a_value > b_value) ? 1 : (a_value < b_value) ? -1 : 0;
            } else {
                return (a_value < b_value) ? 1 : (a_value > b_value) ? -1 : 0;
            }
        });

        if (returnOnly)
            return filtered_data;
        else {
            this[filter.property_name] = filtered_data;
            return filtered_data;
        }
    }


    resetFilter(filter) {
        this[filter.property_name] = this[filter.property_name + "_backup"]
        return this
    }

    resetFilters() {
        this.active_filters.forEach((filter) => {
            this.resetFilter(filter)
        })
        return this
    }

    getActiveFilters() {
        return this.active_filters;
    }

    activateFilters() {
        for (let filter of this.deactivatedFilters) {
            this.setFilter(filter);
        }
    }

    deactivateFilters() {
        this.deactivatedFilters = JSON.parse(JSON.stringify(this.getActiveFilters()));

        this.active_filters = [];
    }

    updateFilterBackups(filter = undefined) {
        if(filter) {
            this[filter.property_name + "_backup"] = this[filter.property_name];
            return
        } else {
            for (let filter of this.active_filters) {
                this[filter.property_name + "_backup"] = this[filter.property_name];
            }
        }
    }
}