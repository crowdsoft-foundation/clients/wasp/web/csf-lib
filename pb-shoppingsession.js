import {makeId, fireEvent, getCookie, setCookie, isJsonString} from "./pb-functions.js?v=[cs_version]";
import {PbVideoconference} from "./pb-videoconference.js?v=[cs_version]";
import {socketio} from "./pb-socketio.js?v=[cs_version]";

export class pbShoppingSession {
    constructor() {
        if (pbShoppingSession.instance) {
            return pbShoppingSession.instance;
        }

        pbShoppingSession.instance = this;
        pbShoppingSession.instance.namespace = "/"
        pbShoppingSession.instance.queue = []
        pbShoppingSession.instance.conference = null
        pbShoppingSession.instance.roomName = ""
        pbShoppingSession.instance.customerName = ""
        pbShoppingSession.instance.merchant = ""
        pbShoppingSession.instance.interest = ""
        pbShoppingSession.instance.requestTime = ""
        pbShoppingSession.instance.interval = null
        return pbShoppingSession.instance

    }

    request(merchant, customerName, interest = "", interval = null, videoHtmlNodeId = "meet", videoWidth = "100%", videoHeight = "100%") {
        if (pbShoppingSession.instance.conference == null) {
            let previousSessionData = decodeURIComponent(getCookie("shoppingsession_data"))

            if(isJsonString(previousSessionData)) {
                previousSessionData = JSON.parse(previousSessionData)
                pbShoppingSession.instance.roomName = previousSessionData.roomName
            }

            if(!pbShoppingSession.instance.roomName || pbShoppingSession.instance.roomName.length == 0) {
                pbShoppingSession.instance.roomName = decodeURIComponent("tmp_" + makeId(16))
            }

            pbShoppingSession.instance.customerName = customerName
            pbShoppingSession.instance.merchant = merchant
            pbShoppingSession.instance.interest = interest
            pbShoppingSession.instance.videoHtmlNodeId = videoHtmlNodeId
            pbShoppingSession.instance.videoWidth = videoWidth
            pbShoppingSession.instance.videoHeight = videoHeight
            pbShoppingSession.instance.interval = interval
            pbShoppingSession.instance.requesttime = Math.round((new Date()).getTime() / 1000)

            new PbVideoconference(pbShoppingSession.instance.roomName).init()
            new socketio().socket.emit('join', {room: pbShoppingSession.instance.roomName});
            pbShoppingSession.instance.conference = new PbVideoconference(pbShoppingSession.instance.roomName, "#" + videoHtmlNodeId, pbShoppingSession.instance.customerName, videoWidth, videoHeight).init()
        }
        pbShoppingSession.instance.interest = interest

        let sessionData = {
            "roomName": pbShoppingSession.instance.roomName,
            "customerName": pbShoppingSession.instance.customerName,
            "merchant": pbShoppingSession.instance.merchant,
            "interest": pbShoppingSession.instance.interest,
        }
        setCookie("shoppingsession_data", JSON.stringify(sessionData))

        fireEvent("newShopSessionRequest", {
            "customer_name": customerName,
            "customer_interest": pbShoppingSession.instance.interest,
            "time": pbShoppingSession.instance.requesttime,
            "room_id": decodeURIComponent(pbShoppingSession.instance.roomName)
        }, pbShoppingSession.instance.merchant)

    }

    reconnectToConference() {
        pbShoppingSession.instance.conference = new PbVideoconference(
            pbShoppingSession.instance.roomName,
            "#" + pbShoppingSession.instance.videoHtmlNodeId,
            pbShoppingSession.instance.customerName,
            pbShoppingSession.instance.videoWidth,
            pbShoppingSession.instance.videoHeight).init()
    }

    connectToConference(roomName, videoHtmlNodeId = "meet", displayName = getCookie("username"), videoWidth = "100%", videoHeight = "100%") {
        new socketio().socket.emit('join', {room: roomName});
        pbShoppingSession.instance.roomName = decodeURIComponent(roomName)
        pbShoppingSession.instance.customerName = displayName
        pbShoppingSession.instance.conference = new PbVideoconference(pbShoppingSession.instance.roomName, "#" + videoHtmlNodeId, pbShoppingSession.instance.customerName, videoWidth, videoHeight).init()
    }

    acceptedBy(srid, username) {
        fireEvent("shopSessionAcceptedBy", {
            "supporter_name": username,
            "srid": srid
        }, getCookie("consumer_name"))
    }

    abortQueuing(reason = "") {
        fireEvent("shopSessionQueuingAborted", {
            "reason": reason,
        }, pbShoppingSession.instance.roomName)
    }

    aboutToLeave() {
        fireEvent("customerInRoomIsLeaving", {
            "room_id": decodeURIComponent(pbShoppingSession.instance.roomName)
        }, pbShoppingSession.instance.merchant)
    }

    left() {
        fireEvent("customerInRoomHasLeft", {
            "room_id": decodeURIComponent(pbShoppingSession.instance.roomName)
        }, pbShoppingSession.instance.merchant)
        pbShoppingSession.instance.quit("left", pbShoppingSession.instance.merchant)
    }

    addToQueue(data) {
        console.log("Current Shopping queue", pbShoppingSession.instance.queue)
        pbShoppingSession.instance.queue.push(data)
        console.log("Current Shopping queue after push", pbShoppingSession.instance.queue)
    }

    connectNextCustomer() {
        console.log("Current Shopping queue", pbShoppingSession.instance.queue)
    }

    quit(reason = "[session_end]", room_id = pbShoppingSession.instance.roomName) {
        fireEvent("shopSessionQuit", {
            "time": Math.round((new Date()).getTime() / 1000),
            "reason": reason,
            "room_id": room_id
        }, room_id)
    }

    cleanUp(clearDatatable=true, clearOrderNotices=true) {

        new socketio().socket.emit('leave', {room: pbShoppingSession.instance.roomName});
        console.log("CLeaning UP", pbShoppingSession.instance)
        new socketio().socket.emit('leave', {room: pbShoppingSession.instance.roomName});
        if (pbShoppingSession.instance.interval != null) {
            clearInterval(pbShoppingSession.instance.interval)
        }
        if (pbShoppingSession.instance.conference) {
            pbShoppingSession.instance.conference.hangup()
        }

        setCookie("shoppingsession_data", "")

        $(window).off('beforeunload');

        if (new URLSearchParams(window.location.search).has("mid")) {
            if ($('#redirectAfterSesssion').data('endsessionredirect')) {
                location.href = $('#redirectAfterSesssion').data('endsessionredirect')
            } else {
                $.blockUI({
                    message: '<div class="sessionEndMessage">Vielen Dank für Ihren Einkauf.</div>',
                    timeout: 5000
                });
            }
        }
        if(clearDatatable) {
            $("#basket").DataTable().clear().draw()
        }
        if(clearOrderNotices) {
            $("#order_notices").val("")
        }
        $(".active-chat-message").remove()

    }

    disconnectCustomer(reason = "[session_end]") {
        fireEvent("shopSessionRemoteAbortCustomer", {
            "time": Math.round((new Date()).getTime() / 1000),
            "reason": reason
        }, pbShoppingSession.instance.roomName)
    }

    requestOrderConfirmation() {
        fireEvent("shopSessionRequestOrderConfirmationFromCustomer", {
            "time": Math.round((new Date()).getTime() / 1000),
            "reason": reason
        }, pbShoppingSession.instance.roomName)
    }

    confirmOrder() {
        fireEvent("shopSessionOrderRequestConfirmedByCustomer", {
            "customer_name": customerName,
            "time": Math.round((new Date()).getTime() / 1000),
            "roomId": decodeURIComponent(pbShoppingSession.instance.roomName)
        }, pbShoppingSession.instance.merchant)
    }

    rejectOrder() {
        fireEvent("shopSessionOrderRequestRejectedByCustomer", {
            "customer_name": customerName,
            "time": Math.round((new Date()).getTime() / 1000),
            "roomId": decodeURIComponent(pbShoppingSession.instance.roomName)
        }, pbShoppingSession.instance.merchant)
    }

    updateNotepad(content) {
        fireEvent("shopSessionOrderNotepadUpdate", {
            "content": content,
        }, pbShoppingSession.instance.roomName)
    }

    start() {
        if (pbShoppingSession.instance.interval != null) {
            clearInterval(pbShoppingSession.instance.interval)
        }
    }
}