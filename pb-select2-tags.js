import {CONFIG} from "../pb-config.js?v=[cs_version]"
import {getCookie, showNotification} from "./pb-functions.js?v=[cs_version]"


export class PbSelect2Tags {
    constructor(selector) {
        this.selector = selector
    }

    setTagsDropdownFromApi(selectedValues = []) {
        let self = this
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_tag_list",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (result) => {
                    self.setTagsDropdown(result, selectedValues)
                    resolve(result)
                },
                "error": () => showNotification("Oops something went wrong, please try again.")
            }

            $.ajax(settings)
        })
    }

    setTagsDropdown(values, selectedValues = [], clear = true) {
        if (clear === true) {
            $(this.selector).html("")
        }

        for (let value of values) {
            this.addTagToDropdown(value.name, value.value, true)
        }
        $(this.selector).trigger('change')
        $(this.selector).select2({
            tags: true,
            tokenSeparators: [',', ' '],
            dropdownParent: $(this.selector).parent()
        });
        this.setSelectedValues(selectedValues)
        return this
    }

    addTagToDropdown(name, value, skip_change_trigger = false) {
        if ($(`${this.selector} option[value="${value}"]`).length === 0) {
            $(this.selector).append($('<option>', {
                value: value,
                text: name
            }))

            if (!skip_change_trigger) {
                $(this.selector).trigger('change')
            }
        }

        return this
    }

    setSelectedValues(selectedValues) {
        for (let tag of selectedValues) {
            if (tag.length > 0 && $(`${this.selector} option[value="${tag}"]`).length === 0) {
                this.addTagToDropdown(tag, tag, true)
            }
        }
        $(this.selector).val(selectedValues).trigger('change')
        return this
    }

    getSelectedValues() {
        return $(this.selector).val()
    }
}