import {PbNotifications} from "./pb-notifications.js?v=[cs_version]"
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {pbI18n} from "./pb-i18n.js?v=[cs_version]"

export class PbErrorhandler {
    constructor() {
        if (!PbErrorhandler.instance) {
            PbErrorhandler.instance = this;
        }

        return PbErrorhandler.instance;
    }

    handleAjaxErrors() {
        $.ajaxSetup({
            error: function (event, request, settings) {
                if (event.status >= 400 && event.status < 500) {
                    PbNotifications.instance.unblockAll()
                    PbNotifications.instance.ask(new pbI18n().translate("Sorry, we were unable to load needed data. We'd like to reload the page in order to prevent further errors."), new pbI18n().translate("Ooops...") + event.status, () => {
                        location.reload(true)
                    })
                } else if (event.status >= 500 && event.status < 600) {
                    PbNotifications.instance.unblockAll()
                    PbNotifications.instance.ask(new pbI18n().translate("Sorry, we were unable to load needed data. We'd like to reload the page in order to prevent further errors."), new pbI18n().translate("Ooops...") + event.status, () => {
                        location.reload(true)
                    })
                } else {
                    PbNotifications.instance.unblockAll()
                    PbNotifications.instance.ask(new pbI18n().translate("We found an unexpected behaviour in the application. If you noticed unnormal behaviour as well, please let us know. Do you want to report this to us so we can investigate further?"), new pbI18n().translate("Ooops...") + event.status, () => {
                        if(CONFIG.ERRORREPORTING.MODE.includes("post")){
                            PbErrorhandler.instance.postError(`ajax-status: ${event.status}`, this, `url:${this.url}`)
                            PbNotifications.instance.showAlert(new pbI18n().translate("Thank you for the report. We will investigate the incident."))
                        }
                    })
                }
            }
        });
    }

    handleWindowErrors() {
        window.onerror = function (message, file, line, col, error) {
            PbNotifications.instance.unblockAll()
            /*PbNotifications.instance.ask(new pbI18n().translate("Wir haben einen unerwarteten Fehler entdeckt. Möchten sie diesen jetzt an uns melden?"), new pbI18n().translate("Ooops..."), () => {
                if(CONFIG.ERRORREPORTING.MODE.includes("post")){
                    PbErrorhandler.instance.postError(message, file, line, col)
                    PbNotifications.instance.showAlert(new pbI18n().translate("Vielen Dank für die Meldung. Wir werden den Fehler so schnell wie möglich beheben. Wenn für Sie alles funktioniert, können sie jetzt weiterarbeiten."))
                }
            })*/
            return false;
        }
    }

    handleUnhandledRejections() {
        window.addEventListener("unhandledrejection", function (e) {
            PbNotifications.instance.unblockAll()
            /*PbNotifications.instance.ask(new pbI18n().translate("Wir haben einen unerwarteten Fehler entdeckt. Möchten sie diesen jetzt an uns melden?"), new pbI18n().translate("Ooops..."), () => {
                if(CONFIG.ERRORREPORTING.MODE.includes("post")){                  
                    PbErrorhandler.instance.postError(`Unhandled rejection on ${window.location.href}`, PbErrorhandler.instance.stringify_object(e))
                    PbNotifications.instance.showAlert(new pbI18n().translate("Vielen Dank für die Meldung. Wir werden den Fehler so schnell wie möglich beheben. Wenn für Sie alles funktioniert, können sie jetzt weiterarbeiten."))
                } 
            })*/
            return false
        })
    }

    postError (message, details="", file="", line="", col="") {
        return new Promise(function (resolve, reject) {
            const error =  JSON.stringify({
                "message": message,
                "file": file,
                "line": line,
                "col": col,
                "details": details
             })
             console.log(error)
             resolve()
            var settings = {
                "url": CONFIG.ERRORREPORTING.POST_URL,
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json",
                },
                "data": error,
                "success": (response) => {
                    resolve(response["task_id"])
                },
                "error": reject()
            };

            $.ajax(settings)
        })
    }
    
    stringify_object(object, depth=0, max_depth=2) {
        // change max_depth to see more levels, for a touch event, 2 is good
        if (depth > max_depth)
            return 'Object';
    
        const obj = {};
        for (let key in object) {
            let value = object[key];
            if (value instanceof Node)
                // specify which properties you want to see from the node
                value = {id: value.id};
            else if (value instanceof Window)
                value = 'Window';
            else if (value instanceof Object)
                value = PbErrorhandler.instance.stringify_object(value, depth+1, max_depth);
    
            obj[key] = value;
        }
    
        return depth? obj: JSON.stringify(obj);
    }
}