import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbQuill} from "./editors/pb-quill.js?v=[cs_version]"

export class PbEditor {
    constructor(type, targetSelector, onUploadStart =()=>"", onUploadFinished =()=>"", options=undefined) {
        let self = this
        return new Promise((resolve, reject)=>
        {
            const available_editors = ["quill", "froala"]
            self.editor = undefined

            if (available_editors.includes(type))
                self.type = type
            else
                throw `Currently only editorType/-s ${available_editors.join(",")} are available`

            switch (self.type) {
                case "quill":
                    self.editor = new PbQuill(targetSelector, options)
                    resolve(self.editor)
                    break
                case "froala":
                    import('./editors/pb-froala.js?v=[cs_version]').then((PbFroala) => {
                        self.editor = new PbFroala.PbFroala(targetSelector, onUploadStart, onUploadFinished, options)
                        resolve(self.editor)
                    })
                    break
            }
        })
    }
}
