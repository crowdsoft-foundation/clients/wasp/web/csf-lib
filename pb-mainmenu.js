import "./pb-eventlistener.js?v=[cs_version]";
import {PbTpl} from "./pb-tpl.js?v=[cs_version]";
import {PbAccount} from '/csf-lib/pb-account.js?v=[cs_version]';
import {getCookie} from "./pb-functions.js?v=[cs_version]"
import {pbI18n} from '/csf-lib/pb-i18n.js?v=[cs_version]';

export class pbMainmenu {
    constructor() {
        let i18n = new pbI18n();

        if (!pbMainmenu.instance) {
            this.menuConfig = [
                {
                    "id": "easy2schedule-card",
                    "permission": "easy2schedule",
                    "title": "CS:Kalender",
                    "icon": '<ion-icon style="font-size: 24px;" name="calendar"></ion-icon>',
                    "description": "Online Kalender für die einfache Termin-/Arbeits-Organisation.",
                    "links": [
                        {
                            "icon_class": "far fa-calendar-alt",
                            "link_text": "Your calendar",
                            "link_href": "/easy2schedule/",
                            "permission": "easy2schedule"
                        },
                        {
                            "icon_class": "fa fa-mobile-alt",
                            "link_text": "Your mobile calendar",
                            "link_href": "/easy2schedule/list-view.html",
                            "permission": "easy2schedule.views.mobile",
                        },
                        {
                            "icon_class": "far fa-clock",
                            "link_text": "Quick booking",
                            "link_href": "/easy2schedule/booking_form.html",
                            "permission": "easy2schedule.bookings",
                        },
                        {
                            "icon_class": "fas fa-tasks",
                            "link_text": "My bookings",
                            "link_href": "/easy2schedule/booking.html",
                            "permission": "easy2schedule.bookings",
                        },
                        {
                            "icon_class": "fas fa-file-export",
                            "link_text": "Data export",
                            "link_href": "",
                            "link_override": '<a id="easy2schedule_link_export_main_menu" data-toggle="modal" data-target="#dataExportModal" href="#" class="tx-dark-blue" data-i18n="Data export">Data export</a>',
                            "permission": "easy2schedule.export",
                        },
                        {
                            "icon_class": "fas fa-file-import",
                            "link_text": "Data import",
                            "link_href": "/easy2schedule/import.html",
                            "permission": "easy2schedule.import",
                        },
                        {
                            "icon_class": "far fa-chart-bar",
                            "link_text": "Statistics",
                            "link_href": "/easy2schedule/statistics.html",
                            "permission": "easy2schedule.statistics",
                        }
                    ]
                },
                {
                    "id": "contactmanager-card",
                    "permission": "frontend.contactmanager",
                    "title": "CS:Kontakte",
                    "icon": "<ion-icon style=\"font-size: 30px;\" name=\"contact\"></ion-icon>",
                    "description": "Einfaches Erfassen und Verwalten von Adressen und Kontaktdaten.",
                    "links": [
                        {
                            "icon_class": "far fa-address-book",
                            "link_text": "Your contacts",
                            "link_href": "/contact-manager/",
                            "permission": "frontend.contactmanager.views"
                        },
                        {
                            "icon_class": "fa fa-archive",
                            "link_text": "Archived contacts",
                            "link_href": "/contact-manager/archived.html",
                            "permission": "frontend.contactmanager.views.archive",
                        },
                        {
                            "icon_class": "far fa-chart-bar",
                            "link_text": "Statistics",
                            "link_href": "/contact-manager/statistics.html",
                            "permission": "frontend.contactmanager.views.statistics"
                        }
                    ]
                },
                {
                    "id": "messaging-center-card",
                    "permission": "frontend.messagingCenter.dashboard",
                    "title": "CS:Messaging-Center",
                    "icon": '<ion-icon style="font-size: 24px;" name="chatboxes"></ion-icon>',
                    "description": "Empfange interne und externe Meldungen / Nachrichten (z.B: E-Mails) oder versende diese direkt hier.",
                    "links": [
                        {
                            "icon_class": "fa fa-list",
                            "link_text": "Messages",
                            "link_href": "/apps/messaging-center/",
                            "permission": "frontend.messagingCenter"
                        },
                        {
                            "icon_class": "fa fa-cogs",
                            "link_text": "Configuration",
                            "link_href": "/apps/messaging-center/config/",
                            "permission": "frontend.messagingCenter.configuration"
                        },
                        {
                            "icon_class": "far fa-file-alt",
                            "link_text": "Templates",
                            "link_href": "/apps/messaging-center/messaging-templates/",
                            "permission": "frontend.messagingCenter.templates"
                        },
                        {
                            "icon_class": "fa fa-reply-all",
                            "link_text": "Quick Reply Configuration",
                            "link_href": "/apps/messaging-center/quick-snippets/",
                            "permission": "beta_access"
                        }
                    ]
                },
                {
                    "id": "taskmanager-card",
                    "permission": "frontend.taskmanager.views",
                    "title": "CS:Aufgaben",
                    "icon": "<ion-icon style=\"font-size: 30px;\" name=\"checkbox-outline\"></ion-icon>",
                    "description": "Zuweisen, Verwalten und Überwachen von Aufgaben für dich und dein Team.",
                    "links": [
                        {
                            "icon_class": "fas fa-tasks",
                            "link_text": "Your Tasks",
                            "link_href": "/task-manager/",
                            "permission": "frontend.taskmanager.views"
                        },
                        {
                            "icon_class": "fas fa-search",
                            "link_text": "Task-Search",
                            "link_href": "/task-manager/tasksearch.html",
                            "permission": "frontend.taskmanager.views.search"
                        }
                    ]
                },
                {
                    "id": "documentation-card",
                    "permission": "frontend.documentation.dashboard",
                    "title": "CS:Dokumentation",
                    "icon": "<ion-icon style=\"font-size: 30px;\" name=\"clipboard\"></ion-icon>",
                    "description": "Einfaches Erfassen und Verwalten von Dokumentationen.",
                    "links": [
                        {
                            "icon_class": "far fa-file-alt",
                            "link_text": "Your documentations",
                            "link_href": "/documentation/index.html",
                            "permission": "frontend.documentation.dashboard.documentation_link"
                        },
                        {
                            "icon_class": "far fa-file-alt",
                            "link_text": "Your callcenter",
                            "link_href": "/documentation/callcenter.html",
                            "permission": "frontend.documentation.dashboard.callcenter_link"
                        },
                        {
                            "icon_class": "fa fa-archive",
                            "link_text": "Archived documents",
                            "link_href": "/documentation/archived.html",
                            "permission": "frontend.documentation.views.archive"
                        },
                        {
                            "icon_class": "far fa-chart-bar",
                            "link_text": "Statistics",
                            "link_href": "/documentation/statistics.html",
                            "permission": "frontend.documentation.dashboard.statistics"
                        }
                    ]
                },
                {
                    "id": "crm-card",
                    "permission": "frontend.crm.dashboard",
                    "title": "CS:CRM",
                    "icon": "<ion-icon style=\"font-size: 30px;\" name=\"filing\"></ion-icon>",
                    "description": "Aufgaben effizient zuweisen, Kunden direkt beraten und Dokumentationen einfach verwalten.",
                    "links": [
                        {
                            "icon_class": "fa fa-list",
                            "link_text": "Task overview",
                            "link_href": "/apps/crm/",
                            "permission": "beta_access"
                        }
                    ]
                },
                {
                    "id": "questionnaire-card",
                    "permission": "frontend.questionaire.dashboard",
                    "title": "CS:Questionaire",
                    "icon": "<ion-icon style=\"font-size: 30px;\" name=\"text\"></ion-icon>",
                    "description": "Fragebögen einsehen, exportieren und verwalten.",
                    "links": [
                        {
                            "icon_class": "fa fa-comment",
                            "link_text": "My questionaires",
                            "link_href": "/questionaire/my_questionaires.html",
                            "permission": "frontend.questionaire.dashboard.my_questionaires"
                        },
                        {
                            "icon_class": "far fa-file-alt",
                            "link_text": "Manage questionaires",
                            "link_href": "/questionaire/questionaire_editor.html",
                            "permission": "frontend.questionaire.dashboard.questionaire_editor"
                        },
                        {
                            "icon_class": "fa fa-download",
                            "link_text": "Export questionaires",
                            "link_href": "/questionaire/questionaire_export.html",
                            "permission": "frontend.questionaire.dashboard.questionaire_export_link"
                        },
                        {
                            "icon_class": "far fa-chart-bar",
                            "link_text": "Statistics",
                            "link_href": "/questionaire/questionaire_statistics.html",
                            "permission": "frontend.questionaire.dashboard.statistics"
                        }
                    ]
                },
                {
                    "id": "statistic-center-card",
                    "permission": "beta_access",
                    "title": "CS:Statistic-Center",
                    "icon": '<ion-icon style="font-size: 24px;" name="stats"></ion-icon>',
                    "description": "Einstellungen, Administration und mehr",
                    "links": [
                        {
                            "icon_class": "fa fa-cogs",
                            "link_text": "Functions",
                            "link_href": "/apps/statistic-center/function-manager",
                            "permission": "beta_access"
                        }
                    ]
                },
                {
                    "id": "cruises-card",
                    "permission": "beta_access",
                    "title": "CS:Cruises",
                    "icon": "<ion-icon style=\"font-size: 30px;\" name=\"boat\"></ion-icon>",
                    "description": "Liste der Angebote.",
                    "links": [
                        {
                            "icon_class": "fa fa-list",
                            "link_text": "Offer overview",
                            "link_href": "/cruises/cruises_contracts.html",
                            "permission": "beta_access"
                        }
                    ]
                },
                {
                    "id": "eventmanager-card",
                    "permission": "frontend.eventmanager",
                    "title": "CS:Veranstaltungen",
                    "icon": "<ion-icon style=\"font-size: 30px;\" name=\"contacts\"></ion-icon>",
                    "description": "Verwalte und teile deine Veranstaltungen online.",
                    "links": [
                        {
                            "icon_class": "fas fa-calendar-check",
                            "link_text": "Your events",
                            "link_href": "/eventmanager/",
                            "permission": "frontend.eventmanager"
                        },
                        {
                            "icon_class": "far fa-chart-bar",
                            "link_text": "Statistics",
                            "link_href": "/eventmanager/statistics.html",
                            "permission": "frontend.eventmanager.statistics"
                        }
                    ]
                },
                {
                    "id": "easy2track-card",
                    "permission": "easy2track",
                    "title": "Easy2Track",
                    "icon": "<img src=\"/style-global/img/icons/track.png\" alt=\"\">",
                    "description": "Covid19 elektronische Kontaktnachverfolgung für Besucher und Gäste.",
                    "links": [
                        {
                            "icon_class": "fas fa-qrcode",
                            "link_text": "QR-Code",
                            "link_href": "/easy2track/client.html",
                            "permission": "easy2track.qrcode"
                        },
                        {
                            "icon_class": "far fa-file-alt",
                            "link_text": "Guest formular",
                            "link_href": "/easy2track/index.html",
                            "permission": "easy2track.form"
                        },
                        {
                            "icon_class": "fas fa-mobile-alt",
                            "link_text": "Scanner",
                            "link_href": "/easy2track/scanner.html",
                            "permission": "easy2track.scanner"
                        },
                        {
                            "icon_class": "far fa-chart-bar",
                            "link_text": "Statistics",
                            "link_href": "/easy2track/visitors.html",
                            "permission": "easy2track.statistics"
                        }
                    ]
                },
                {
                    "id": "easy2order-card",
                    "permission": "easy2order",
                    "title": "Easy2Order",
                    "icon": "<img src=\"/style-global/img/icons/order.png\" alt=\"\">",
                    "description": "Gäste Bestellungen organisieren, einfach und schnell.",
                    "links": [
                        {
                            "icon_class": "fas fa-qrcode",
                            "link_text": "QR Generator",
                            "link_href": "/prototypes/easy2order/qrgen.html",
                            "permission": "easy2order"
                        },
                        {
                            "icon_class": "fas fa-tasks",
                            "link_text": "Order-Entry",
                            "link_href": "/prototypes/easy2order/entries.html",
                            "permission": "easy2order"
                        }
                    ]
                },
                {
                    "id": "simplycollect-card",
                    "permission": "simplycollect",
                    "title": "Simplycollect",
                    "icon": "<img src=\"/style-global/img/icons/chat.png\" alt=\"\">",
                    "description": "Echtzeit Kundenberatung mit Videounterstützung und Warenkorbfunktion.",
                    "links": [
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "Händleransicht",
                            "link_href": "/simplycollect/merchant.html",
                            "permission": "simplycollect"
                        },
                        {
                            "icon_class": "fas fa-shopping-cart",
                            "link_text": "Kundenwebseite",
                            "link_href": "/simplycollect/customer.html?mid=demo",
                            "permission": "simplycollect"
                        },
                        {
                            "icon_class": "fas fa-info-circle",
                            "link_text": "Simplycollect Information",
                            "link_href": "/simplycollect/index.html",
                            "permission": "simplycollect"
                        }
                    ]
                },
                {
                    "id": "demo-card",
                    "permission": "frontend.demo_right",
                    "title": "Demo-Card",
                    "icon": "<img src=\"/style-global/img/icons/chat.png\" alt=\"\">",
                    "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
                    "links": [
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu Roommanagement",
                            "link_href": "/prototypes/roommanagement/",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu Simpledocs",
                            "link_href": "/prototypes/simpledocs/",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu Roommanagement",
                            "link_href": "/prototypes/roommanagement/",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu Easy2Order",
                            "link_href": "/prototypes/easy2order/",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu DID",
                            "link_href": "/prototypes/did/",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu Timetracker",
                            "link_href": "/timetracker/",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu Easy2Gether",
                            "link_href": "/easy2gether/",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-store-alt",
                            "link_text": "zu planBLICK",
                            "link_href": "https://www.planblick.com",
                            "permission": "frontend.demo_right"
                        },
                        {
                            "icon_class": "fas fa-shopping-cart",
                            "link_text": "planB Suite",
                            "link_href": "https://planb-suite.de/",
                            "permission": "frontend.demo_right"
                        }
                    ]
                }
            ]

            pbMainmenu.instance = this;
        }

        return pbMainmenu.instance;
    }

    async resortConfigToStoredOrder() {
        await new PbAccount().init(getCookie("consumer_id"))
        let dashboard_settings = PbAccount.instance.getAccountProfileData()["dashboard_settings"]
        if (dashboard_settings) {
            dashboard_settings = JSON.parse(dashboard_settings)
            let idArray = dashboard_settings.map(item => item.id);
            this.menuConfig = this.menuConfig.sort((a, b) => idArray.indexOf(a.id) - idArray.indexOf(b.id));
        }
        return pbMainmenu.instance;
    }

    async render() {
        await this.resortConfigToStoredOrder()
        await new PbTpl().init().renderIntoAsync("snippets/mainmenu/header-apps-menu.html", {"config": this.menuConfig}, "#header_apps_menu")

        window.jQuery("#header_apps_menu").click(function (event) {
            window.jQuery("#header_apps_menu").addClass("show");
        });

        window.jQuery("#easy2schedule_link_export_main_menu").click(function (event) {
            event.preventDefault(); // Prevent default link behavior
            event.stopPropagation();

            // Ensure modal exists
            if ($("#dataExportModal").length) {
                $("#dataExportModal").modal("show"); // Manually trigger modal
            } else {
                console.error("Modal not found in the DOM!");
            }
        });


        let toggles = document.querySelectorAll(".toggle-submenu");

        toggles.forEach(toggle => {
            toggle.addEventListener("click", (event) => {
                if (!event.target.closest("a")) {
                    event.preventDefault();

                    let submenu = toggle.querySelector(".submenu");

                    if (submenu) {
                        const isHidden = submenu.classList.contains("hidden");

                        document.querySelectorAll(".submenu:not(.hidden)").forEach(openMenu => {
                            if (openMenu !== submenu) {
                                openMenu.classList.add("hidden");
                            }
                        });

                        if (isHidden) {
                            submenu.classList.remove("hidden");
                        } else {
                            submenu.classList.add("hidden");
                        }
                    } else {
                        console.error("Submenu not found!");
                    }
                }
            });
        });
    }
}