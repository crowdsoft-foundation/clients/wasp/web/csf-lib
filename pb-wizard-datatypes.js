export class DtWizardScenario {
    name;
    steps;
    constructor(name, steps) {
        this.name = name;
        this.steps = steps;
        return this;
    }
    addStep(step) {
        this.steps.push(step);
    }
    getStep(stepNumber) {
        return this.steps.filter((step) => {
            return step.step_number == stepNumber;
        })[0];
    }
}
export class DtWizardStep {
    step_number;
    shortname;
    title;
    subtitle;
    bodytemplate;
    onload;
    onleave;
    constructor(step_number, shortname, title, subtitle, bodytemplate, onload, onleave) {
        this.step_number = step_number;
        this.shortname = shortname;
        this.title = title;
        this.subtitle = subtitle;
        this.bodytemplate = bodytemplate;
        this.onload = onload;
        this.onleave = onleave;
        return this;
    }
}
//# sourceMappingURL=pb-wizard-datatypes.js.map
