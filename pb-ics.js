export class PbIcs {
    constructor() {
        // Initialize any attributes or configurations here
    }

    downloadIcs(name="meeting", summary, description, location, start, end) {
        console.log("PbICS DaTA",name, summary, description, location, start, end)
        const eventDetails = {
            summary: summary,
            description: description,
            start: start,
            end: end,
            location: location,
        };

// Create the ICS file content
        const icsContent = `BEGIN:VCALENDAR
PRODID:-//Tabnine//NONSGML v1.0//EN
CALSCALE:GREGORIAN
BEGIN:VEVENT
UID:${eventDetails.start.toISOString()}
DTSTART;TZID=UTC:${eventDetails.start.toISOString().replace(' ', 'T')}
DTEND;TZID=UTC:${eventDetails.end.toISOString().replace(' ', 'T')}
SUMMARY:${eventDetails.summary}
DESCRIPTION:${eventDetails.description}
LOCATION:${eventDetails.location}
END:VEVENT
END:VCALENDAR`;

        const icsBlob = new Blob([icsContent], {type: 'text/calendar'});
        const icsUrl = window.URL.createObjectURL(icsBlob);

        const icsLink = document.createElement('a');
        icsLink.href = icsUrl;
        icsLink.download = name + '.ics';

        document.body.appendChild(icsLink);
        icsLink.click();
        document.body.removeChild(icsLink);
        window.URL.revokeObjectURL(icsUrl);
    }
}