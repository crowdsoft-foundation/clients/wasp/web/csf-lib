export class PbTaskHandler {
  constructor() {
    if (PbTaskHandler.instance) {
      return PbTaskHandler.instance;
    }
    this.tasks = {};
    PbTaskHandler.instance = this;
  }

  addTaskId(id) {
    if (!this.tasks[id]) {
      this.tasks[id] = () => {}; // Initially setting an empty function
    }
  }

  setTaskMethod(id, method) {
    if (typeof method === "function") {
      if (this.tasks[id] && typeof this.tasks[id] !== "function") {
          console.log("TASKHANDLER set-calling function", id)
        let args = this.tasks[id];
        this.tasks[id] = method
        this.execute(id, args)
      } else {
        console.log("TASKHANDLER setting function", id)
        this.tasks[id] = method
      }
    } else {
      console.error("Provided method is not a function.");
    }
  }

  execute(taskId, args={}) {
    if (this.tasks[taskId] && typeof this.tasks[taskId] === "function") {
      this.tasks[taskId](args);
      console.log("TASKHANDLER found function", args)
      return true
    } else {
      this.tasks[taskId] = args
      console.log("TASKHANDLER haven't found function", args)
      return false
    }
  }
}


